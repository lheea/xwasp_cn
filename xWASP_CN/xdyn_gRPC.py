#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module provides classes for an interactive interface between Python with X-dyn, more specifically through the 'xdyn-for-cs' executable (gRPC server).

Classes
-------
Xdyn_server
    This class is a Python interface to xdyn-for-cs. It manages the communication with the 'xdyn-for-cs' gRPC server.
"""

from abc import ABC, abstractmethod
import time
from math import floor, isnan
from copy import copy, deepcopy
from os import O_NONBLOCK
from fcntl import fcntl, F_SETFL
import subprocess as sp
from tempfile import NamedTemporaryFile
from typing import Type, List, Dict, Set, Callable

import yaml
import portpicker
import numpy as np
from scipy.optimize import root
from scipy.spatial.transform import Rotation
import grpc

import cosimulation_pb2_grpc
import cosimulation_pb2
import model_exchange_pb2_grpc
import model_exchange_pb2

from xWASP_CN.errors import InvalidInputError, OperationError, ExecutionError, InvalidValueError
from xWASP_CN.data_structures import SystemState, ForceLabel, SystemStateHistory
import xWASP_CN.xdyn_gRPC_plots as plots
from xWASP_CN.yaml_helpers import get_yaml

np.seterr(invalid='raise')

# numpy.float64 support for YAML
def numpy_float64_representer(dumper, value):
    """Add a numpy.float64 representer to yaml library dumper."""
    return dumper.represent_scalar('tag:yaml.org,2002:float', repr(float(value)).lower())
yaml.add_representer(np.float64, numpy_float64_representer)


class XdynInstance(ABC):
    """Abstract class that provides an interface to either xdyn-for-cs or xdyn-for-me. Responsible for starting the executable and creating a gRPC client (channel + stub)."""
    def __init__(self, yaml_input, config, input_path):
        self._port = portpicker.pick_unused_port()
        # self.local_input_dir = Path(input_path) / Path("input_"+str(self._port))
        # self.local_input_dir.mkdir(parents=True, exist_ok=True)
        # local_input_yaml = self.local_input_dir / "input.yaml"
        # with local_input_yaml.open(mode='w') as file:
        #     yaml.dump(yaml_input, file)
        file = NamedTemporaryFile(mode='w+t', suffix='.yml') #local_input_yaml.relative_to(Path(input_path)).as_posix(),
        yaml.dump(yaml_input, file)
        self.command = [self._get_executable(), file.name,
                        '--port', str(self._port),
                        '-g']
        self.command = self.command + self._get_executable_options(config)
        self._address = '127.0.0.1'
        self.process = sp.Popen(self.command, cwd=input_path, stdout=sp.PIPE, stderr=sp.STDOUT, universal_newlines=True, text=True)
        # self.process = sp.Popen(self.command, cwd=input_path, universal_newlines=True, text=True)

        stdout = self.process.stdout.readline()
        time.sleep(2)
        if self.process.poll() is not None:
            line=self.process.stdout.readline()
            while line:
                stdout+=line
                line=self.process.stdout.readline()
            raise ExecutionError('Error occured when launching '+self._get_executable()+' with port '+str(self._port)+': '+stdout)

        fcntl(self.process.stdout.fileno(), F_SETFL, O_NONBLOCK) # This sets the stdout of the xdyn-for-me process as non-blocking

        self._channel = grpc.insecure_channel(self._address+":"+str(self._port)).__enter__()
        # def info(status): # TODO: include this in logging
        #     print("Channel status: ", status)
        # self._channel.subscribe(info, try_to_connect=True)
        self._stub = self._get_stub(self._channel)

    @abstractmethod
    def _get_stub(self, channel):
        pass

    @abstractmethod
    def _get_executable(self):
        pass

    @abstractmethod
    def _stub_call(self, request):
        pass

    def execute(self, request):
        self.health_check()
        ret = self._stub_call(request)
        self.flush_pipe_buffers()
        return ret

    @abstractmethod
    def _get_executable_options(self, config):
        pass

    def flush_pipe_buffers(self):
        """Flushes the content of the subprocess's stdout/stderr pipe to avoid overflowing.
        """
        lines = self.process.stdout.readlines()
        while len(lines)>0:
            lines = self.process.stdout.readlines()

    def health_check(self):
        if self.process.poll() is not None:
            lines = self.process.stdout.readlines()
            if lines:
                info = lines[-1]
            else:
                info = "None"
            raise ExecutionError("The xdyn executable stopped unexpectedly, latest stdout/stderr: "+str(info))

    def __del__(self):
        if hasattr(self, '_channel'):
            self._channel.close()
        if hasattr(self, 'process'):
            self.process.terminate()

class XdynCSinstance(XdynInstance):
    """Provides an interface for xdyn-for-cs (Co-Simulation)"""
    def _get_stub(self, channel):
        return cosimulation_pb2_grpc.CosimulationStub(channel)

    def _get_executable(self):
        return 'xdyn-for-cs'

    def _stub_call(self, request: cosimulation_pb2.CosimulationRequestEuler):
        return self._stub.step_euler_321(request, wait_for_ready=True, timeout=max(10*request.Dt, 10.))

    def _get_executable_options(self, config):
        return ['--solver',str(config['solver']),
                '--dt', str(config['dt'])]

class XdynMEinstance(XdynInstance):
    """Provides an interface for xdyn-for-me (Model Exchange)"""
    def _get_stub(self, channel):
        return model_exchange_pb2_grpc.ModelExchangeStub(channel)

    def _get_executable(self):
        return 'xdyn-for-me'

    def _stub_call(self, request: model_exchange_pb2.ModelExchangeRequestEuler):
        return self._stub.dx_dt_euler_321(request, wait_for_ready=True, timeout=10.)

    def _get_executable_options(self, config):
        return []

# Main interface to xdyn
class Xdyn_server:
    """This class is a general Python interface for xdyn.

    This class provides a number of useful functions to communicate with xdyn and display its results.
    This class must be used as a context manager (or an explicit call to __enter__ must be made).
    """
    def __init__(self, input_yaml, config: dict, input_path, mode: Type[XdynInstance] = XdynCSinstance):
        self._yaml = get_yaml(input_yaml)

        if len(self._yaml['bodies']) > 1:
            raise InvalidInputError('input_yaml', self._yaml,
                             'Xdyn_server (which relies on xdyn-for-cs) currently only works for a single body. The simulation that input you provided has several bodies.')

        self.mode = mode
        self.config = config
        self.input_path = input_path
        self.body_name = str(self._yaml['bodies'][0]['name'])
        self._xdyn_instance = None

        self._all_force_labels = self._build_all_force_labels()

    def _build_all_force_labels(self):
        # TODO: Use ForceLabels instead of strings
        force_labels = set()
        forces = (self._yaml['bodies'][0]['external forces'] if 'external forces' in self._yaml['bodies'][0] else []) + (self._yaml['bodies'][0]['controlled forces'] if 'controlled forces' in self._yaml['bodies'][0] else [])
        for force in forces:
            if 'name' in force:
                force_name = str(force['name'])
            else:
                force_name = str(force['model'])
            for frame in [self.body_name, 'NED']:
                for component in ['Fx','Fy','Fz','Mx','My','Mz']:
                    force_labels.add(component+'('+force_name+','+self.body_name+','+frame+')')
        for frame in [self.body_name, 'NED']:
            for component in ['Fx','Fy','Fz','Mx','My','Mz']:
                force_labels.add(component+'(sum of forces,'+self.body_name+','+frame+')')
                force_labels.add(component+'(fictitious forces,'+self.body_name+','+frame+')')
        for component in ['Fx','Fy','Fz','Mx','My','Mz']:
            force_labels.add(component+'(blocked states,'+self.body_name+','+self.body_name+')')

        return force_labels

    def __enter__(self):
        self._xdyn_instance = self.mode(self._yaml, self.config, self.input_path)
        return self

    def __exit__(self, exc_type=None, exc_value=None, exc_traceback=None):
        del self._xdyn_instance
        self._xdyn_instance = None

        if exc_type:
            return False
        return True

    def __del__(self):
        self.__exit__()

    def check_running(self, raise_exception: bool = False):
        """Check that there are a client a running server."""
        if (self._xdyn_instance):
            return True
        else:
            if raise_exception:
                raise OperationError('The server must be launched and connected to perform this action.')
            else:
                return False

    def execute(self, request):
        """Execute the request and return the response.

        Parameters
        ----------
        request: cosimulation_pb2.CosimulationRequestEuler or model_exchange_pb2.ModelExchangeRequestEuler
            Structure containing the previous states and the simulation time.

        Returns
        -------
        cosimulation_pb2.CosimulationResponse or model_exchange_pb2.ModelExchangeResponse
            Structure containing the simulated states and requested outputs.
        """
        return self._xdyn_instance.execute(request)


class Xdyn_server_steady(Xdyn_server):
    """This class is a Python interface for xdyn-for-cs.

    It provides functions to use in standard root-finding and optimization algorithms. It also includes the blocking of some degrees of freedom.
    """
    def __init__(self, input_yaml, config: dict, input_path):
        super().__init__(input_yaml, config, input_path, mode = XdynMEinstance)
        self.simulation_states_labels = {'u','v','w','p','q','r','x','y','z','phi','theta','psi'}

    def sum_of_forces(self, states: Dict[str, float], axes: List[str] = None, global_frame: bool = False) -> np.ndarray:
        """Return the sum of forces for a set of given states. This function is intended to be used in standard root-finding algorithms.
        This function is const and does not modify the results stored in the class.

        Parameters
        ----------
        states: Dict[str, float]
            State values.
        axes: List[str], optional
            Axes to include in the returned array, among ['Fx','Fy','Fz','Mx','My','Mz']. The returned array is ordered according to this parameter. Defaults to all axis.
        global_frame: bool, optional
            Returns the sum of forces in the NED frame instead of the body frame. Defaults to False.

        Returns
        -------
        nd.ndarray:
            Sum of forces for all the components specified in parameter 'axis'
        """
        if global_frame:
            frame = 'NED'
        else:
            frame = self.body_name
        if axes is None:
            axes = ['Fx','Fy','Fz','Mx','My','Mz']
        request = self._make_request(states, False)
        self._add_sum_of_forces_to_requested_output(request, axes, frame)
        response = self.execute(request)
        sum_of_forces = np.zeros(len(axes), dtype=float)
        for i,axis in enumerate(axes):
            SF_label = ForceLabel(axis, 'sum of forces', self.body_name, frame)
            sum_of_forces[i] = response.extra_observations[SF_label.as_xdyn()]
            if isnan(sum_of_forces[i]):
                request = self._make_request(states, True)
                response = self.execute(request)
                possible_culprits = set()
                for label, force in response.extra_observations.items():
                    if isnan(force):
                        force_label = ForceLabel.from_xdyn(label)
                        if force_label.name!='sum of forces' and force_label.axis==axis and force_label.frame==frame:
                            possible_culprits.add(force_label.name)
                raise InvalidValueError(SF_label.as_xdyn(), 'Possible culprit force models are: '+', '.join(possible_culprits)+'. States were: '+str(states))
        return sum_of_forces

    def _callback(self, callback, states, labels, blocked_dofs, response):
        results = {}
        for label in self._all_force_labels:
            results[label] = response.extra_observations[label]
        callback(states, labels, blocked_dofs, results)

    def _make_request(self, states: Dict[str, float], request_all_forces: bool = False):
        states = copy(states)
        self._solve_for_missing_variables(states)

        request = model_exchange_pb2.ModelExchangeRequestEuler()
        request.states.t[:] = [0.]
        request.states.u[:] = [states['u']]
        request.states.v[:] = [states['v']]
        request.states.w[:] = [states['w']]
        request.states.p[:] = [states['p']]
        request.states.q[:] = [states['q']]
        request.states.r[:] = [states['r']]
        request.states.x[:] = [states['x']]
        request.states.y[:] = [states['y']]
        request.states.z[:] = [states['z']]
        request.states.phi[:] = [states['phi']]
        request.states.theta[:] = [states['theta']]
        request.states.psi[:] = [states['psi']]
        for label, value in states.items():
            if label not in {'u','v','w','p','q','r','x','y','z','phi','theta','psi','dx_dt','dy_dt','dz_dt'}:
                request.commands[label] = value
        if request_all_forces:
            request.requested_output[:] = self._all_force_labels
        return request

    def _add_sum_of_forces_to_requested_output(self, request: model_exchange_pb2.ModelExchangeRequestEuler, components: Set[str], frame: str):
        for axis in components:
            label = axis+'(sum of forces,'+self.body_name+','+frame+')'
            self._add_requested_output(request, label)

    def _add_requested_output(self, request: model_exchange_pb2.ModelExchangeRequestEuler, label: str):
        if label not in request.requested_output:
            request.requested_output.append(label)

    def _solve_for_missing_variables(self, known_variables: Dict[str, float]):
        missing_states = self.simulation_states_labels.difference(set(known_variables))
        n_unknown = len(missing_states)
        if n_unknown<=3 and missing_states.issubset({'u','v','w','phi','theta','psi'}):
            n_known_additional = len({'dx_dt','dy_dt','dz_dt'}.intersection(set(known_variables)))
            if n_unknown==n_known_additional:
                unknown_additional = {'dx_dt','dy_dt','dz_dt'}.difference(set(known_variables))
                indexes = list(missing_states.union(unknown_additional)) # this maps each missing variable to an index in the vector to be solved
                def f_to_solve(x):
                    phi = known_variables['phi'] if 'phi' in known_variables else x[indexes.index('phi')]
                    theta = known_variables['theta'] if 'theta' in known_variables else x[indexes.index('theta')]
                    psi = known_variables['psi'] if 'psi' in known_variables else x[indexes.index('psi')]
                    R = Rotation.from_euler('ZYX', [psi, theta, phi])
                    dx_dt = known_variables['dx_dt'] if 'dx_dt' in known_variables else x[indexes.index('dx_dt')]
                    dy_dt = known_variables['dy_dt'] if 'dy_dt' in known_variables else x[indexes.index('dy_dt')]
                    dz_dt = known_variables['dz_dt'] if 'dz_dt' in known_variables else x[indexes.index('dz_dt')]
                    X_dot = np.array([dx_dt,dy_dt,dz_dt])
                    u = known_variables['u'] if 'u' in known_variables else x[indexes.index('u')]
                    v = known_variables['v'] if 'v' in known_variables else x[indexes.index('v')]
                    w = known_variables['w'] if 'w' in known_variables else x[indexes.index('w')]
                    U = np.array([u,v,w])
                    return X_dot - R.apply(U)
                x_init = [0.,0.,0.]
                res = root(f_to_solve, x_init)
                if res.success and np.linalg.norm(f_to_solve(res.x))<10e-6:
                    for i,label in enumerate(indexes):
                        known_variables[label] = res.x[i]
                else:
                    raise OperationError('Failed to solve for the variables '+str(missing_states)+': \n'+str(res)+'\n where x is '+str(indexes)+' and the following is known: '+str(known_variables))
            elif n_unknown<n_known_additional:
                raise OperationError('The system is over-constrained. '+str(n_unknown)+' state variables among (u,v,w,phi,theta,psi) are missing, and '+str(n_known_additional)+' variables among (dx_dt,dy_dt,dz_dt) are provided.')
            else: # n_unknown>n_known_additional
                raise OperationError('The system is under-constrained. '+str(n_unknown)+' state variables among (u,v,w,phi,theta,psi) are missing, but only '+str(n_known_additional)+' variables among (dx_dt,dy_dt,dz_dt) are provided.')
        elif n_unknown>3 :
            raise OperationError('There are too many unknown variables')
        elif not(missing_states.issubset({'u','v','w','phi','theta','psi'})):
            raise OperationError('The following variables must be provided (either part of the state vector or blocked): '+str(missing_states.difference({'u','v','w','phi','theta','psi'})))

    def _get_velocity_in_NED_frame(self, states: Dict[str, float]):
        R = Rotation.from_euler('ZYX', [states['psi'], states['theta'], states['phi']])
        U = np.array([states['u'], states['v'], states['w']])
        Xdot = R.apply(U)
        return {'dx_dt': Xdot[0], 'dy_dt': Xdot[1], 'dz_dt': Xdot[2]}

    def optimization_objective(self, *args, **kwargs):
        return np.linalg.norm(self.sum_of_forces(*args, **kwargs))

    def get_system_states(self, states: Dict[str, float], requested_output: Set[str] = None) -> SystemState:
        request = self._make_request(states, True)
        if requested_output is not None:
            for label in requested_output:
                self._add_requested_output(request, label)
        response = self.execute(request)
        state = {'u':request.states.u[0],
               'v':request.states.v[0],
               'w':request.states.w[0],
               'p':request.states.p[0],
               'q':request.states.q[0],
               'r':request.states.r[0],
               'x':request.states.x[0],
               'y':request.states.y[0],
               'z':request.states.z[0],
               'phi':request.states.phi[0],
               'theta':request.states.theta[0],
               'psi':request.states.psi[0]}
        state.update(self._get_velocity_in_NED_frame(state))
        forces = {ForceLabel.from_xdyn(label):response.extra_observations[label] for label in self._all_force_labels}
        commands = {}
        for label, value in states.items():
            if label not in {'u','v','w','p','q','r','x','y','z','phi','theta','psi','dx_dt','dy_dt','dz_dt'}:
                commands[label] = value
        if not commands:
            commands = None
        other = None
        if requested_output:
            other = {label:response.extra_observations[label] for label in requested_output}
        return SystemState(state, forces, commands, other)


class Xdyn_server_stateful(Xdyn_server):
    """This class is a Python interface for xdyn-for-cs, that stores the time history of the simulation.

    The executable xdyn-server can be launched directly in the local environment or in a Docker container. This class provides a number of useful functions to communicate with xdyn-server and display its results. It is recommended to use this class as a context manager. Otherwise the server must be launched and the ZMQ connexion set must through the dedicated member functions (respectively 'launch()' and 'connect()') before any interaction with the actual xdyn-server executable.
    """

    def __init__(self, input_yaml, config: dict, input_path, Dt: float = 2., requested_output: Set[str] = None, output_all_forces: bool = False, signals: dict = None, plot: bool = False):
        super().__init__(input_yaml, config, input_path, mode = XdynCSinstance)
        self.Dt = Dt
        self._states = None
        self._extra_observations = {}
        self._init_states_from_yaml()
        self._plot = plot
        self._requested_output = set()
        if output_all_forces:
            self._requested_output.update(self._all_force_labels)
        if requested_output is None:
            requested_output = set()
        self._requested_output.update(requested_output)
        if signals is None:
            self._signals = {}
        else:
            self._signals = signals
        self._requested_output.update(self._signals.keys())

    def _init_states_from_yaml(self):
        self._states = cosimulation_pb2.CosimulationStatesEuler()
        self._states.t[:] = [0.]

        velocity = self._yaml['bodies'][0]['initial velocity of body frame relative to NED']
        self._states.u[:] = [velocity['u']['value']]
        self._states.v[:] = [velocity['v']['value']]
        self._states.w[:] = [velocity['w']['value']]
        self._states.p[:] = [velocity['p']['value']]
        self._states.q[:] = [velocity['q']['value']]
        self._states.r[:] = [velocity['r']['value']]

        position = self._yaml['bodies'][0]['initial position of body frame relative to NED']
        self._states.x[:] = [position['x']['value']]
        self._states.y[:] = [position['y']['value']]
        self._states.z[:] = [position['z']['value']]
        self._states.psi[:] = [position['psi']['value']]
        self._states.theta[:] = [position['theta']['value']]
        self._states.phi[:] = [position['phi']['value']]

    def __enter__(self):
        super().__enter__()
        self.setup_results()
        return self

    def setup_results(self):
        """Makes a first call to the server to get all the requested results at t0 and plot them if requested."""
        request = self._make_request()
        request.Dt = float(self.config['dt'])
        response = self.execute(request)
        self._init_extra_observations(response.extra_observations)
        self.update_plots()

    def update_plots(self):
        """Update existing plots and create new requested ones."""
        if self._plot:
            if isinstance(self._plot, plots.ResultsPlot):
                self._plot.update(self._states, self._extra_observations)
            else:
                self._plot = plots.ResultsPlot(self._states, self.body_name, self._extra_observations)
        self.update_extra_plots()

    def _init_extra_observations(self, extra_observations):
        for obs in extra_observations:
            self._extra_observations[obs] = [extra_observations[obs].value[0]]

    def set_signal(self, name: str, value: float):
        """Sets a signal to a value.

        Parameters
        ----------
        name : str
            Name of the signal to be set
        value : float
            Value of the signal
        """
        self._signals[name] = value

    def set_signals(self, signals: Dict[str, float]):
        """Sets a signal to a value.

        Parameters
        ----------
        name : str
            Name of the signal to be set
        value : float
            Value of the signal
        """
        self._signals.update(signals)

    def _make_request(self):
        """Build the client request from current states."""
        request = cosimulation_pb2.CosimulationRequestEuler()
        request.Dt = self.Dt
        request.states.CopyFrom(self._states) # Cannot directly assign a nested message type
        for name, value in self._signals.items(): # Cannot directly assign a map
            request.commands[name] = value
        request.requested_output[:] = self._requested_output
        return request

    def _handle_response(self, response):
        """Update the current states with the contents of the server response."""
        self._states.t.extend(response.all_states.t[1:])
        self._states.x.extend(response.all_states.x[1:])
        self._states.y.extend(response.all_states.y[1:])
        self._states.z.extend(response.all_states.z[1:])
        self._states.u.extend(response.all_states.u[1:])
        self._states.v.extend(response.all_states.v[1:])
        self._states.w.extend(response.all_states.w[1:])
        self._states.p.extend(response.all_states.p[1:])
        self._states.q.extend(response.all_states.q[1:])
        self._states.r.extend(response.all_states.r[1:])
        self._states.phi.extend(response.all_states.phi[1:])
        self._states.theta.extend(response.all_states.theta[1:])
        self._states.psi.extend(response.all_states.psi[1:])
        for obs in response.extra_observations:
            self._extra_observations[obs].extend(response.extra_observations[obs].value[1:])
        self.update_plots()

    def simulate(self, T):
        """Computes the next T period of time by increments of Dt (attribute), updating the results at each increment.

        Parameters
        ----------
        T : float
            The time period to be simulated

        Returns
        -------
        float
            The execution time
        """
        try:
            T = float(T)
        except:
            raise InvalidInputError("T", T, "Argument should be a float (or convertible).")
        timer = time.time()
        t = 0.
        while t+self.Dt <= T:
            self.step()
            t += self.Dt
        if t < T:
            request = self._make_request()
            request.Dt = T-t
            response = self.execute(request)
            self._handle_response(response)
        return time.time()-timer

    def step(self):
        """Send the current history of states to the xdyn-server server and computes the next Dt period of time.

        Returns
        -------
        float
            The execution time
        """
        timer = time.time()
        request = self._make_request()
        response = self.execute(request)
        self._handle_response(response)
        return time.time()-timer

    def reset(self, state=None):
        """Reset the history of states to the argument 'state' (defaults to the first record in the current history).

        Parameters
        ----------
        state : dict
            The state at which to reset the simulation
        """
        self.check_running(True)
        state = deepcopy(state)
        if state is None:
            state = self.get_initial_state()
        else:
            if not {'t', 'x', 'y', 'z', 'u', 'v', 'w', 'p', 'q', 'r', 'phi', 'theta', 'psi'} <= state.keys():
                raise InvalidInputError("state", state, "All the following keys must be present: 't','x','y','z','u','v','w','p','q','r','phi','theta' and 'psi'.")
            elif not all([(isinstance(state[key], float) or isinstance(state[key], int)) for key in ['t', 'x', 'y', 'z', 'u', 'v', 'w', 'p', 'q', 'r', 'phi', 'theta', 'psi']]):
                raise InvalidInputError("state", state, "The values for the states in variable 'state' must be floating point numbers")
        self._states.t[:] = [state['t']]
        self._states.x[:] = [state['x']]
        self._states.y[:] = [state['y']]
        self._states.z[:] = [state['z']]
        self._states.u[:] = [state['u']]
        self._states.v[:] = [state['v']]
        self._states.w[:] = [state['w']]
        self._states.p[:] = [state['p']]
        self._states.q[:] = [state['q']]
        self._states.r[:] = [state['r']]
        self._states.phi[:] = [state['phi']]
        self._states.theta[:] = [state['theta']]
        self._states.psi[:] = [state['psi']]
        self.setup_results()

    def reset_from_converged_state(self):
        """Reset the history of states from the mean values of the last 10% of the simulation.
        """
        state = self.get_converged_state()
        state.update({'t': 0.})
        self.reset(state)

    def reset_from_mean_state(self):
        """Reset the history of states from the mean values of the entire history of the simulation.
        """
        state = self.get_mean_state()
        state.update({'t': 0.})
        self.reset(state)

    def get_initial_state(self):
        """Get the values of the states at the first recorded time.

        Returns
        -------
        dict
            First recorded state.
        """
        ret = {'t': self._states.t[0],
               'x': self._states.x[0],
               'y': self._states.y[0],
               'z': self._states.z[0],
               'u': self._states.u[0],
               'v': self._states.v[0],
               'w': self._states.w[0],
               'p': self._states.p[0],
               'q': self._states.q[0],
               'r': self._states.r[0],
               'phi': self._states.phi[0],
               'theta': self._states.theta[0],
               'psi': self._states.psi[0]}
        return ret

    def get_state_history(self, state):
        if state == 'x':
            return list(self._states.x)
        elif state == 'y':
            return list(self._states.y)
        elif state == 'z':
            return list(self._states.z)
        elif state == 'u':
            return list(self._states.u)
        elif state == 'v':
            return list(self._states.v)
        elif state == 'w':
            return list(self._states.w)
        elif state == 'p':
            return list(self._states.p)
        elif state == 'q':
            return list(self._states.q)
        elif state == 'r':
            return list(self._states.r)
        elif state == 'phi':
            return list(self._states.phi)
        elif state == 'theta':
            return list(self._states.theta)
        elif state == 'psi':
            return list(self._states.psi)
        else:
            raise InvalidInputError("state", state, 'The requested state is nowhere to be found')

    def get_history(self, variable):
        """Return the time history of a state or extra observation."""
        if variable == 't' or variable == 'time':
            return self.get_time_vector()
        elif variable in ['x','y','z','u','v','w','p','q','r','psi','theta','phi']:
            return self.get_state_history(variable)
        elif variable in self._extra_observations.keys():
            return self._extra_observations[variable]
        else:
            raise InvalidInputError("variable", variable, 'The requested variable is nowhere to be found. Make sure it was requested beforehand.')

    def get_converged_state(self):
        """Get the mean values of the states during the last 10% of the simulation, without convergence check.

        Returns
        -------
        dict
            Converged state (regardless of actual convergence).
        """
        N = len(self._states.t)
        state = {}
        state['x'] = np.mean(self._states.x[floor(9*N/10):])
        state['y'] = np.mean(self._states.y[floor(9*N/10):])
        state['z'] = np.mean(self._states.z[floor(9*N/10):])
        state['u'] = np.mean(self._states.u[floor(9*N/10):])
        state['v'] = np.mean(self._states.v[floor(9*N/10):])
        state['w'] = np.mean(self._states.w[floor(9*N/10):])
        state['p'] = np.mean(self._states.p[floor(9*N/10):])
        state['q'] = np.mean(self._states.q[floor(9*N/10):])
        state['r'] = np.mean(self._states.r[floor(9*N/10):])
        state['phi'] = np.mean(self._states.phi[floor(9*N/10):])
        state['theta'] = np.mean(self._states.theta[floor(9*N/10):])
        state['psi'] = np.mean(self._states.psi[floor(9*N/10):])
        return state

    def get_mean_state(self):
        """Get the mean values of the states during the entire simulation.

        Returns
        -------
        dict
            Mean state.
        """
        state = {}
        state['x'] = np.mean(self._states.x)
        state['y'] = np.mean(self._states.y)
        state['z'] = np.mean(self._states.z)
        state['u'] = np.mean(self._states.u)
        state['v'] = np.mean(self._states.v)
        state['w'] = np.mean(self._states.w)
        state['p'] = np.mean(self._states.p)
        state['q'] = np.mean(self._states.q)
        state['r'] = np.mean(self._states.r)
        state['phi'] = np.mean(self._states.phi)
        state['theta'] = np.mean(self._states.theta)
        state['psi'] = np.mean(self._states.psi)
        return state

    def get_converged_variable(self, variable):
        """Get the mean values of a variable during the last 10% of the simulation, without convergence check.

        Returns
        -------
        dict
            Converged variable (regardless of actual convergence).
        """
        N = len(self._states.t)
        return np.mean(self.get_history(variable)[floor(9*N/10):])

    def get_converged_variables(self):
        """Get the mean values of all variables (states and extra observations) during the last 10% of the simulation, without convergence check.

        Returns
        -------
        dict
            Converged variable (regardless of actual convergence).
        """
        ret = self.get_converged_state()
        N = len(self._states.t)
        for obs, values in self._extra_observations.items():
            ret[obs] = np.mean(values[floor(9*N/10):])
        return ret

    def check_convergence(self, references):
        """Checks if the simulation (current history) has converged according to the criteria of the argument 'references'.

        Parameters
        ----------
        references : dict
            The argument 'references' must be a dict with variable names (forces, states...) as keys and lambda functions as values. Each lambda function must have a signature 'bool(float,float)'. The arguments are respectively the mean value and the standard deviation over the last 10% of time of the simulation history. The key variable is considered converged if the lambda function returns True.

        Returns
        -------
        list[str]
            List of all the variables that did not converge. Empty list if all convergence tests passed.

        """
        not_converged_variables = []
        N = len(self._states.t)
        for var, conv_specs in references.items():
            if len(self.get_time_vector()) > 2:
                data_set = self.get_history(var)[floor(9*N/10):]
                conv = conv_specs(np.mean(data_set), np.std(data_set))
                if not conv:
                    not_converged_variables.append(var)
            else:
                not_converged_variables.append(var)
        return not_converged_variables

    def run_until_convergence(self, references, max_time, check_first: bool = True):
        """Runs the simulation by steps of Dt (attribute) until either 'max_time' is reached or 'check_convergence(references)' is achieved.

        Parameters
        ----------
        references : dict
            The argument 'references' must be a dict with variable names (forces, states...) as keys and functions as values. Each function must have a signature 'bool(float,float)'. The arguments are respectively the mean value and the standard deviation over the last 10% of time of the simulation history. The key variable is considered converged if the function returns True.
        max_time : float
            Maximum simulation time

        Returns
        -------
        list[str]
            Returns an empty list if the convergence was achieved, or a list of the variables that did not converge if 'max_time' was reached without convergence.
        """
        t0 = self.get_time()
        if check_first:
            cond = self.check_convergence(references)
        else:
            cond = [1]
        while self.get_time()-t0 < max_time and cond != []:
            self.step()
            cond = self.check_convergence(references)
        return cond

    def check_conditions(self, conditions: List[Callable[[Callable[[str], List[float]]], bool]]):
        """Checks if the simulation (current history) meets all the conditions described in the argument 'conditions'.

        Parameters
        ----------
        conditions : list[callable]
            The argument 'conditions' must be a dict with labels as keys and functions (callables) as values. Each function must have a signature 'bool(history_getter)'. The history_getter passed to the function returns a list of floats containing the history of the variables given in argument. The condition is considered met if the function returns True.

        Returns
        -------
        bool
            Whether all conditions were met or not.
        """
        valid = True
        for condition in conditions:
            try:
                valid = valid and condition(self.get_history)
            except Exception as error:
                raise OperationError("The function provided for the conditional test is invalid. Its signature must be 'bool(history_getter)'. The following error occured: "+str(error)) from error
        return valid

    def run_until_conditions(self, conditions, max_time, check_first: bool = True):
        """Runs the simulation by steps of Dt (attribute) until either 'max_time' is reached or 'check_conditions(conditions)' is achieved.

        Parameters
        ----------
        conditions : list[callable]
            The argument 'conditions' must be a dict with labels as keys and functions (callables) as values. Each function must have a signature 'bool(history_getter)'. The history_getter passed to the function returns a list of floats containing the history of the variables given in argument. The condition is considered met if the function returns True.
        max_time : float
            Maximum simulation time

        Returns
        -------
        list[str]
            Returns an empty list if the conditions were met, or a list of the variables that did not pass the check if 'max_time' was reached without all conditions being met at the final time.
        """
        t0 = self.get_time()
        if check_first:
            cond = self.check_conditions(conditions)
        else:
            cond = False
        while self.get_time()-t0 < max_time and not(cond):
            self.step()
            cond = self.check_conditions(conditions)
        return cond

    def plot(self, set_live=False):
        """Plot the current results of the simulation.

        Parameters
        ----------
        set_live : bool
            If argument 'set_live' is True, the plots will be updated as the simulation goes on and this function does not return (as if the argument 'plot' was True during initialization). Otherwise (default), this function returns an instance of LiveResultsPlot with plots until the current time, but these will not be updated automatically when the simulation continues.

        Returns
        -------
        LiveResultsPlot
            If the argument set_live is False, returns an instance of LiveResultsPlot containing the results up to the current time and their plots.
        """
        plot = plots.ResultsPlot(self._states)
        if set_live:
            self._plot=plot
        else:
            return plot

    def custom_plot(self, x_var, y_vars: List[str]):
        """Plot the specified variables.

        Parameters
        ----------
        x_var : str
            Name of the abscissa variable
        y_vars : list
            Name of the ordinate variables as a list of strings

        """
        if isinstance(y_vars, str):
            y_vars = [y_vars]
        y_values = []
        for var in y_vars:
            y_values.append(self.get_history(var))
        if isinstance(self._plot, plots.ResultsPlot):
            self._plot.custom_plot(x_var, self.get_history(x_var), y_vars, y_values)
        else:
            raise OperationError('The plot mode must be activated in order to plot additional data.')

    def update_extra_plots(self):
        if isinstance(self._plot, plots.ResultsPlot):
            self._plot.update_custom_plots(self.get_history)

    def get_converged(self, var):
        N = len(self._states.t)
        return np.mean(self.get_history(var)[floor(9*N/10):])

    def get_time_vector(self):
        """Returns the list of time stamps."""
        return list(self._states.t)

    def get_time(self):
        """Get the current (latest) phydical time of the simulation.

        Returns
        -------
        float
            Latest record time (current time)
        """
        return self.get_time_vector()[-1]

    def get_force(self, force, axe, frame):
        """Get a the history of a force variable from its label, axis and frame."""
        return self.get_history(axe+"("+force+","+self.body_name+","+frame+")")

    def get_all(self) -> SystemStateHistory:
        """Get the histories of all recorded variables (including states) as a SystemStateHistory."""
        time_vector = self.get_time_vector()
        states = {}
        for label in ['x','y','z','u','v','w','p','q','r','psi','theta','phi']:
            states[label] = self.get_state_history(label)
        forces = {}
        for label in self._requested_output:
            try:
                forces[ForceLabel.from_xdyn(label)] = self.get_history(label)
            except InvalidInputError:
                pass
        commands = None
        if self._signals:
            commands = {}
            for label in self._signals.keys():
                commands[label] = self.get_history(label)
        return SystemStateHistory(time_vector, states, forces, commands)
