#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import urllib.request
import json
import logging

from xWASP_CN.errors import Error
from xWASP_CN.loggers import StackLogger
logging.setLoggerClass(StackLogger)
import xWASP_CN.main as main


LASTEST_COMMIT_ID = 'latest_commit_id_placeholder' # Overwritten by build process with latest Git commit SHA-1


def cli():
    # Parsing input arguments
    parser = argparse.ArgumentParser(prog = 'xWASP_CN',
                                     description="""xWASP_CN is a Wind-Assisted Ship Propulsion performance prediction tool.
                                                    It was developed at the LHEEA (Ecole Centrale de Nantes/CNRS) with funding from the ADEME and Région Pays de la Loire.
                                                    It is distributed under the Eclipse Public License version 2.0 (see the LICENSE file).""")

    parser.add_argument('input_file',help='Name of the YAML input for the wrapper')
    parser.add_argument('--output','-o',dest='output_file',default=None,help='Name of the HDF5 output file. If no output file is specified, no data will be saved.')
    parser.add_argument('--logging','-l',dest='logging_level',default='INFO',help='Logging level of console output. One of DEBUG, INFO, WARNING, ERROR, CRITICAL (not case sensitive).')

    console_input=parser.parse_args()

    numeric_level = getattr(logging, console_input.logging_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid logging level: %s. Supported values are DEBUG, INFO, WARNING, ERROR and CRITICAL (not case sensitive).' % console_input.logging_level)
    logger = logging.getLogger('xWASP_CN')
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter('[%(levelname)s] %(message)s'))
    stream_handler.setLevel(numeric_level)
    logger.addHandler(stream_handler)

    # Checking version
    if LASTEST_COMMIT_ID != 'missing_hash':
        try:
            gitlab_hash = json.loads(urllib.request.urlopen("https://gitlab.com/api/v4/projects/33151144/repository/commits/master").read())['id']
            if gitlab_hash != LASTEST_COMMIT_ID:
                logging.info("Update available at gitlab.com/lheea/xwasp_cn")
        except Exception as error:
            logging.warning("Failed to check version.")
            logging.debug(str(error))

    # Running each test
    try:
        main.run_wrapper(vars(console_input))
    except Error as error:
        logger.critical(str(error))
    except KeyboardInterrupt:
        logger.critical('The program was manually interrupted.')
    except:
        logger.exception('Unkown error occurred. Please create an Issue at gitlab.com/lheea/xwasp_cn/issues with the details of the error (logs and input files). If the logs/input files cannot be disclosed publicly, please contact the maintainers via email.')


if __name__=='__main__':
    cli()