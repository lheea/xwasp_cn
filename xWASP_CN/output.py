"""This module defines file outputters for xWASP.
"""
import os
from abc import ABC, abstractmethod
import logging

import h5py
from numpy import vectorize, array

from xWASP_CN.errors import InvalidInputError
from xWASP_CN.yaml_helpers import get_yaml
from xWASP_CN.data_structures import ParametersUnits, StaticResults

logging.getLogger('h5py').setLevel(logging.CRITICAL) # Deactivating logging from h5py under CRITICAL level

class Outputter(ABC):
    """This class is an interface for the different outputters.
    """
    @abstractmethod
    def write_input(self, input_data, xdyn_model_file):
        pass

    @abstractmethod
    def write_parameters(self, wind_speeds, wind_angles, units: ParametersUnits):
        pass

    @abstractmethod
    def write_hydrostatics_result(self, hydrostatics_result):
        pass

    @abstractmethod
    def write_statics_results(self, statics_results):
        pass

    @abstractmethod
    def write_dynamic_results(self, dynamic_results):
        pass


def is_UV(dic):
    if isinstance(dic, dict) and 'unit' in dic and len(dic)==2:
        if 'value' in dic:
            return 'value'
        if 'values' in dic:
            return 'values'
    return False

def dict_recursive_write(root: h5py.Group, name: str, data):
    if isinstance(data, dict):
        value_label = is_UV(data)
        if value_label:
            root.create_dataset(str(name), data=data[value_label]).attrs['unit'] = data['unit']
        else:
            next_root = root.require_group(name)
            for label,item in data.items():
                dict_recursive_write(next_root, str(label), item)

    elif isinstance(data, list):
        try:
            root.create_dataset(name, data=data)
        except TypeError as error:
            next_root = root.require_group(name)
            for item in data:
                if isinstance(item, dict):
                    if 'name' in item:
                        dict_recursive_write(next_root, item['name'], item)
                    elif 'model' in item:
                        dict_recursive_write(next_root, item['model'], item)
                    else:
                        raise InvalidInputError('data', item, 'The data cannot be written to HDF5') from error
                else:
                    raise InvalidInputError('data', item, 'The data cannot be written to HDF5') from error

    else:
        try:
            root.create_dataset(name, data=data)
        except TypeError as error:
            raise InvalidInputError('data', data, 'The data cannot be written to HDF5') from error

def uniquify(path):
    """Adds a number inside parenthesis is the file 'path' already exists."""
    filename, extension = os.path.splitext(path)
    counter = 1

    while os.path.exists(path):
        path = filename + "(" + str(counter) + ")" + extension
        counter += 1

    return path

class HDF5Outputter(Outputter):
    """This is an HDF5 file outputter for xWASP.
    """
    def __init__(self, filename: str, overwrite_output_file: bool = False):
        if not overwrite_output_file:
            filename = uniquify(filename)
        self.file = h5py.File(filename, 'w')

        self.wind_speeds_ds = None
        self.wind_angles_ds = None

    def write_input(self, input_data, xdyn_model_file):
        input_grp = self.file.require_group('input')
        dict_recursive_write(input_grp, 'xWASP', input_data)
        # input_grp.create_goup('xWASP', data = yaml.dump(input_data))
        xdyn_model = get_yaml(xdyn_model_file)
        dict_recursive_write(input_grp, 'xdyn', xdyn_model)
        # input_grp.require_dataset('xdyn', data = yaml.dump(xdyn_model))

    def write_parameters(self, wind_speeds, wind_angles, units: ParametersUnits):
        output_grp = self.file.require_group('output')

        self.wind_speeds_ds = output_grp.create_dataset('wind speeds', data=wind_speeds)
        self.wind_speeds_ds.attrs['unit'] = units.wind_speed
        self.wind_speeds_ds.make_scale()

        self.wind_angles_ds = output_grp.create_dataset('wind angles', data=wind_angles)
        self.wind_angles_ds.attrs['unit'] = units.wind_angle
        self.wind_angles_ds.make_scale()

    def _attach_scales(self, dataset: h5py.Dataset):
        dataset.dims[0].attach_scale(self.wind_speeds_ds)
        dataset.dims[0].label = 'wind speed'
        dataset.dims[1].attach_scale(self.wind_angles_ds)
        dataset.dims[1].label = 'wind angle'

    def write_statics_results(self, statics_results):
        output_grp = self.file.require_group('output')
        statics_grp = output_grp.require_group('statics')
        states_units = {'u': 'm/s', 'v': 'm/s', 'w': 'm/s', 'z': 'm', 'phi': 'rad', 'theta': 'rad', 'dx_dt':'m/s', 'dy_dt':'m/s'}
        states_grp = statics_grp.require_group('states')
        for state, unit in states_units.items():
            get_state = vectorize(lambda x: x.system_states.states[state])
            ds = states_grp.create_dataset(state, data=get_state(statics_results))
            ds.attrs['unit'] = unit
            self._attach_scales(ds)

        force_labels = statics_results[0,0].system_states.forces.keys()
        forces_grp = statics_grp.require_group('forces')
        for label in force_labels:
            get_force = vectorize(lambda x: x.system_states.forces[label])
            force_grp = forces_grp.require_group(label.name)
            frame_grp = force_grp.require_group(label.frame)
            ds = frame_grp.create_dataset(label.axis, data=get_force(statics_results))
            if label.is_force():
                ds.attrs['unit'] = 'N'
            elif label.is_moment():
                ds.attrs['unit'] = 'N.m'
            self._attach_scales(ds)

        if statics_results[0,0].system_states.commands is not None:
            command_labels = statics_results[0,0].system_states.commands.keys()
            commands_grp = statics_grp.require_group('commands')
            for label in command_labels:
                get_command = vectorize(lambda x: x.system_states.commands[label])
                ds = commands_grp.create_dataset(label, data=get_command(statics_results))
                self._attach_scales(ds)

        if statics_results[0,0].system_states.other is not None:
            other_labels = statics_results[0,0].system_states.other.keys()
            other_grp = statics_grp.require_group('other')
            for label in other_labels:
                get_other = vectorize(lambda x: x.system_states.other[label])
                ds = other_grp.require_dataset(label, data=get_other(statics_results))
                self._attach_scales(ds)

        if statics_results[0,0].computed_data is not None:
            computed_data_labels = statics_results[0,0].computed_data.keys()
            computed_data_grp = statics_grp.require_group('special')
            for label in computed_data_labels:
                get_computed_data = vectorize(lambda x: x.computed_data[label].value)
                ds = computed_data_grp.create_dataset(label, data=get_computed_data(statics_results))
                ds.attrs['unit'] = statics_results[0,0].computed_data[label].unit
                self._attach_scales(ds)

    def write_hydrostatics_result(self, hydrostatics_result: StaticResults):
        hydrostatics_grp = self.file.require_group('output').require_group('initial hydrostatics')
        states_units = {'u': 'm/s', 'v': 'm/s', 'w': 'm/s','p': 'rad/s', 'q': 'rad/s', 'r': 'rad/s', 'z': 'm', 'phi': 'rad', 'theta': 'rad'}
        states_grp = hydrostatics_grp.require_group('states')
        for state, unit in states_units.items():
            ds = states_grp.create_dataset(state, data=hydrostatics_result.system_states.states[state])
            ds.attrs['unit'] = unit

        forces_grp = hydrostatics_grp.require_group('forces')
        for label, value in hydrostatics_result.system_states.forces.items():
            force_grp = forces_grp.require_group(label.name)
            frame_grp = force_grp.require_group(label.frame)
            ds = frame_grp.create_dataset(label.axis, data=value)
            if label.is_force():
                ds.attrs['unit'] = 'N'
            elif label.is_moment():
                ds.attrs['unit'] = 'N.m'

        if hydrostatics_result.system_states.commands is not None:
            commands_grp = hydrostatics_grp.require_group('commands')
            for label, value in hydrostatics_result.system_states.commands.items():
                ds = commands_grp.create_dataset(label, data=value)

        if hydrostatics_result.system_states.other is not None:
            other_grp = hydrostatics_grp.require_group('other')
            for label, value in hydrostatics_result.system_states.other.items():
                ds = other_grp.require_dataset(label, data=value)

        if hydrostatics_result.computed_data is not None:
            computed_data_grp = hydrostatics_grp.require_group('special')
            for label, uv_pair in hydrostatics_result.computed_data.items():
                ds = computed_data_grp.create_dataset(label, data=uv_pair.value)
                ds.attrs['unit'] = uv_pair.unit

    def write_dynamic_results(self, dynamic_results):
        output_grp = self.file.require_group('output')
        dynamics_grp = output_grp.require_group('dynamics')
        for i in range(dynamic_results.shape[2]):
            test_name = dynamic_results[0,0,i].test_name
            test_grp = dynamics_grp.require_group(test_name)

            computed_data_grp = test_grp.require_group('special')
            computed_data_labels = dynamic_results[0,0,i].computed_data.keys()
            for label in computed_data_labels:
                get_computed_data = vectorize(lambda x: x.computed_data[label].value)
                ds = computed_data_grp.create_dataset(label, data=get_computed_data(dynamic_results[:,:,i]))
                ds.attrs['unit'] = dynamic_results[0,0,i].computed_data[label].unit
                self._attach_scales(ds)

            if dynamic_results[0,0,i].time_series is not None:
                time_series_grp = test_grp.require_group('time series')
                dt = h5py.vlen_dtype(float)
                get_time = vectorize(lambda x: array(x.time_series.time, dtype=float), otypes=[dt])
                ds = time_series_grp.create_dataset('time', data=get_time(dynamic_results[:,:,i]), dtype=dt)
                ds.attrs['unit'] = 's'
                self._attach_scales(ds)

                states_units = {'u': 'm/s', 'v': 'm/s', 'w': 'm/s','p': 'rad/s', 'q': 'rad/s', 'r': 'rad/s', 'x': 'm', 'y': 'm', 'z': 'm', 'phi': 'rad', 'theta': 'rad', 'psi': 'rad'}
                states_grp = time_series_grp.require_group('states')
                for state, unit in states_units.items():
                    get_state = vectorize(lambda x: array(x.time_series.states[state], dtype=float), otypes=[dt])
                    ds = states_grp.create_dataset(state, data=get_state(dynamic_results[:,:,i]), dtype=dt)
                    ds.attrs['unit'] = unit
                    self._attach_scales(ds)

                force_labels = dynamic_results[0,0,i].time_series.forces.keys()
                forces_grp = time_series_grp.require_group('forces')
                for label in force_labels:
                    get_force = vectorize(lambda x: array(x.time_series.forces[label], dtype=float), otypes=[dt])
                    force_grp = forces_grp.require_group(label.name)
                    frame_grp = force_grp.require_group(label.frame)
                    ds = frame_grp.create_dataset(label.axis, data=get_force(dynamic_results[:,:,i]), dtype=dt)
                    if label.is_force():
                        ds.attrs['unit'] = 'N'
                    elif label.is_moment():
                        ds.attrs['unit'] = 'N.m'
                    self._attach_scales(ds)

                if dynamic_results[0,0,i].time_series.commands is not None:
                    command_labels = dynamic_results[0,0,i].time_series.commands.keys()
                    commands_grp = time_series_grp.require_group('commands')
                    for command in command_labels:
                        get_command = vectorize(lambda x: array(x.time_series.commands[command], dtype=float), otypes=[dt])
                        ds = commands_grp.create_dataset(command, data=get_command(dynamic_results[:,:,i]), dtype=dt)
                        self._attach_scales(ds)


class CSVOutputter(Outputter):
    """This is a CSV file outputter for xWASP.
    """
    pass
