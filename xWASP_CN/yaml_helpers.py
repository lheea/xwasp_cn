import re
from os.path import isfile
from os import PathLike
from copy import deepcopy

from numpy import pi, radians
import yaml

import xWASP_CN.errors as errors
from xWASP_CN.data_structures import Parameters, Mode, sigRE

yaml_loader = yaml.SafeLoader
yaml_loader.add_implicit_resolver(
    u'tag:yaml.org,2002:float',
    re.compile(u'''^(?:[-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?|[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)|\\.[0-9_]+(?:[eE][-+][0-9]+)?|[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*|[-+]?\\.(?:inf|Inf|INF)|\\.(?:nan|NaN|NAN))$''',
               re.X),
    list(u'-+0123456789.'))

def get_yaml(input_yaml):
    """Parses a YAML file into a dict. Input may be a string, a PathLike or an already parsed dict (in which case it just return a copy of the argument)."""
    if (isinstance(input_yaml,str) or isinstance(input_yaml,PathLike)):
        if isfile(input_yaml):
            with open(input_yaml, 'r', encoding='utf-8') as file:
                return yaml.load(file, Loader=yaml_loader)
        else:
            raise errors.InvalidInputError('input_yaml',input_yaml,'Input YAML file not found.')
    elif isinstance(input_yaml,dict):
        return deepcopy(input_yaml)
    else:
        raise errors.InvalidInputError('input_yaml',input_yaml,'The provided YAML input is not valid: it must be either a dict instance, a YAML-formatted string, a file name or a file identifier.')

def check_yaml_for_forces(xdyn_model_file, forces, input_name=None):
    yaml_input = get_yaml(xdyn_model_file)
    force_models = []
    if 'controlled forces' in yaml_input['bodies'][0]:
        force_models.extend(yaml_input['bodies'][0]['controlled forces'])
    if 'external forces' in yaml_input['bodies'][0]:
        force_models.extend(yaml_input['bodies'][0]['external forces'])
    for force_name in forces:
        force_found = False
        for model in force_models:
            if 'name' in model:
                if model['name'] == force_name:
                    force_found = True
            elif model['model'] == force_name:
                force_found = True
        if not force_found:
            raise errors.InvalidInputError(input_name if input_name else 'force_name', force_name, "Force model not found in the YAML")

def convert_wind_direction(true_wind_angle: float, unit: str = 'deg') -> float:
    """Converts a True Wind Angle to a wind direction in NED frame (xdyn convention) for a ship heading due North.

    Parameters
    ----------
    true_wind_angle : float
        True Wind Angle (in deg) for a ship heading due North.
    unit : str
        Angle unit, either 'deg' or 'rad'. Defaults to 'deg'.

    Returns
    -------
    float
        Wind direction according to the convention in xdyn, in the unit specified in the parameters.
    """
    if unit == 'deg':
        offset = 180
    elif unit == 'rad':
        offset = pi
    else:
        raise errors.InvalidInputError("unit", unit, "Invalid angle unit. Only 'deg' and 'rad' are supported.")
    TWA = true_wind_angle%(2*offset)
    return (TWA + offset)%(2*offset)

def get_modified_yaml(yaml_input, parameters: Parameters, initial_state=None, waves=None, wind_model: dict = None):
    """Sets the environment in the xdyn model input according to the parameters, waves, and initial state.
       Returns a copy (the model given as argument is not modified).
    """
    xdyn_model = deepcopy(yaml_input)
    wind_model = deepcopy(wind_model)
    waves = deepcopy(waves)
    initial_state = deepcopy(initial_state)
    wind_mean_velocity = {'unit': parameters.units.wind_speed,
                          'value': parameters.wind_speed}
    wind_direction = {'unit': parameters.units.wind_angle,
                      'value': convert_wind_direction(parameters.wind_angle, parameters.units.wind_angle)} #Conversion from NED frame to ship frame when heading North (0°), so that wind_direction refers to the origin direction of wind rather than the direction of its flow
    if wind_model is None:
        wind_model = {'model': 'uniform wind',
                      'velocity': wind_mean_velocity,
                      'direction': wind_direction}
    else:
        wind_model['velocity'] = wind_mean_velocity
        wind_model['direction'] = wind_direction
    if waves is None:
        waves={'model': 'no waves',
               'constant sea elevation in NED frame': {'value': 0, 'unit': 'm'}}
    xdyn_model = set_environment_in_yaml(xdyn_model, wind_model, waves)
    if initial_state:
        xdyn_model = set_initial_state_in_yaml(xdyn_model, initial_state)
    return xdyn_model

def set_environment_in_yaml(yaml_input, wind_model, waves_model):
    yaml_input['environment models'] = [wind_model, waves_model]
    return yaml_input

def set_command_in_yaml(yaml_input,command,value):
    match = sigRE.match(command)
    if not match:
        raise errors.InvalidInputError('command',command,"The provided command does not match a command name structure 'target(variable)'")
    if not is_UV(value):
        raise errors.InvalidInputError('value',value,"The provided value should be a dict containing a unit key and a value key")
    command_found=False
    if not 'commands' in yaml_input:
        yaml_input['commands']=[]
    for command in yaml_input['commands']:
        if command['name']==match.group('target'):
            command[match.group('variable')]={'unit':value['unit'], 'values':[value['value']]*len(command['t'])}
            command_found=True
            break
    if not command_found:
        yaml_input['commands'].append({'name':match.group('target'),'t':[0.],match.group('variable'):{'unit':value['unit'], 'values':[value['value']]}})
    return yaml_input

def set_initial_state_in_yaml(yaml_input,state,distance_unit='m',angle_unit='rad',time_unit='s'):
    linear_velocity_unit=distance_unit+'/'+time_unit
    angular_velocity_unit=angle_unit+'/'+time_unit
    init_vel={'frame':yaml_input['bodies'][0]['name'],
              'u':{'unit':linear_velocity_unit, 'value':state['u']},
              'v':{'unit':linear_velocity_unit, 'value':state['v']},
              'w':{'unit':linear_velocity_unit, 'value':state['w']},
              'p':{'unit':angular_velocity_unit,'value':state['p']},
              'q':{'unit':angular_velocity_unit,'value':state['q']},
              'r':{'unit':angular_velocity_unit,'value':state['r']}}
    yaml_input['bodies'][0]['initial velocity of body frame relative to NED']=init_vel
    init_pos={'frame':'NED',
              'x':{'unit':distance_unit, 'value':state['x']},
              'y':{'unit':distance_unit, 'value':state['y']},
              'z':{'unit':distance_unit, 'value':state['z']},
              'phi':{'unit':angle_unit, 'value':state['phi']},
              'theta':{'unit':angle_unit, 'value':state['theta']},
              'psi':{'unit':angle_unit, 'value':state['psi']}}
    yaml_input['bodies'][0]['initial position of body frame relative to NED']=init_pos
    return yaml_input

def set_initial_velocity_in_yaml(yaml_input,u,unit='m/s'):
    yaml_input['bodies'][0]['initial velocity of body frame relative to NED']['u']={'unit':unit, 'value':u}
    return yaml_input

def block_DoF_in_yaml(yaml_input: dict, DoF: str, value: float, max_time: float):
    if not DoF in ['u','v','w','p','q','r']:
        raise errors.InvalidInputError('DoF',DoF,"DoF must be one of the following: 'u','v','w','p','q' or 'r'")
    blocked_dof_section = {'state':DoF,'t':[0,float(max_time)],'value':[float(value),float(value)],'interpolation':'piecewise constant'}
    if 'blocked dof' in yaml_input['bodies'][0]:
        if 'from YAML' in yaml_input['bodies'][0]['blocked dof']:
            for i,blocked_dof in enumerate(yaml_input['bodies'][0]['blocked dof']['from YAML']):
                if blocked_dof['state']==DoF:
                    yaml_input['bodies'][0]['blocked dof']['from YAML'][i] = blocked_dof_section
                    break
            else:
                yaml_input['bodies'][0]['blocked dof']['from YAML'].append(blocked_dof_section)
        else:
            yaml_input['bodies'][0]['blocked dof']['from YAML']=[blocked_dof_section]
        if 'from CSV' in yaml_input['bodies'][0]['blocked dof']:
            for i,blocked_dof in enumerate(yaml_input['bodies'][0]['blocked dof']['from CSV']):
                if blocked_dof['state']==DoF:
                    yaml_input['bodies'][0]['blocked dof']['from CSV'].pop(i)
                    break
            if yaml_input['bodies'][0]['blocked dof']['from CSV']==[]:
                del yaml_input['bodies'][0]['blocked dof']['from CSV']
    else:
        yaml_input['bodies'][0]['blocked dof'] = {'from YAML':[blocked_dof_section]}
    return yaml_input

def block_yaw_in_yaml(yaml_input, max_time):
    return block_DoF_in_yaml(yaml_input,'r',0.,max_time)

def enforce_speed_in_yaml(yaml_input, speed, max_time):
    return block_DoF_in_yaml(yaml_input,'u',speed,max_time)

def add_simple_heading_controller(yaml_input: dict, T: float, ksi: float, add_command: bool = True):
    force_models = []
    if 'controlled forces' in yaml_input['bodies'][0]:
        force_models.extend(yaml_input['bodies'][0]['controlled forces'])
    if 'external forces' in yaml_input['bodies'][0]:
        force_models.extend(yaml_input['bodies'][0]['external forces'])
    else:
        yaml_input['bodies'][0]['external forces'] = []
    for force_model in force_models:
        if force_model['model'] == 'simple heading controller':
            raise errors.OperationError("Simple heading controller already present in the model.")
    simple_heading_controller = {'name': 'heading_controller',
                                 'model': 'simple heading controller',
                                 'ksi': ksi,
                                 'Tp': {'value': T, 'unit': 's'}}
    yaml_input['bodies'][0]['external forces'].append(simple_heading_controller)
    if add_command:
        yaml_input = set_command_in_yaml(yaml_input, 'heading_controller(psi_co)', {'value': 0., 'unit': 'deg'})
    return yaml_input

def add_constant_propulsion_force(yaml_input, propulsion_force):
    body_name = yaml_input['bodies'][0]['name']
    constant_force_model = {'model': 'constant force',
                            'frame': body_name,
                            'x': {'value': 0., 'unit': 'm'},
                            'y': {'value': 0., 'unit': 'm'},
                            'z': {'value': 0., 'unit': 'm'},
                            'X': {'value': propulsion_force, 'unit': 'N'},
                            'Y': {'value': 0., 'unit': 'N'},
                            'Z': {'value': 0., 'unit': 'N'},
                            'K': {'value': 0., 'unit': 'N*m'},
                            'M': {'value': 0., 'unit': 'N*m'},
                            'N': {'value': 0., 'unit': 'N*m'}}
    if 'external forces' in yaml_input['bodies'][0]:
        yaml_input['bodies'][0]['external forces'].append(constant_force_model)
    else:
        yaml_input['bodies'][0]['external forces'] = [constant_force_model]
    return yaml_input

def get_initial_attitude(yaml_input):
    """Returns the initial attitude (sinkage, heel and trim, i.e. z, phi and theta) from the xdyn model, or None if the data is missing from the model."""
    if 'initial position of body frame relative to NED' in yaml_input['bodies'][0]:
        states = {}
        if yaml_input['bodies'][0]['initial position of body frame relative to NED']['z']['unit'] != 'm':
            raise errors.InvalidInputError('initial position of body frame relative to NED: z', yaml_input['bodies'][0]['initial position of body frame relative to NED']['z'], "Only supported unit is 'm' (meter)")
        states['z'] = yaml_input['bodies'][0]['initial position of body frame relative to NED']['z']['value']
        states['phi'] = get_as_radians(yaml_input['bodies'][0]['initial position of body frame relative to NED']['phi'])
        states['theta'] = get_as_radians(yaml_input['bodies'][0]['initial position of body frame relative to NED']['theta'])
        return states
    else:
        return None

def parse_angle_unit(angle_unit):
    string=str(angle_unit)
    if string=='deg' or string=='rad':
        return string
    else:
        raise errors.InvalidInputError('angle_unit',angle_unit,"Invalid angle unit: it must be either 'rad' or 'deg'")

def get_as_radians(uv_pair: dict) -> float:
    unit = parse_angle_unit(uv_pair['unit'])
    if unit == 'rad':
        return float(uv_pair['value'])
    elif unit == 'deg':
        return radians(float(uv_pair['value']))

def is_UV(node):
    if not ('unit' in node and 'value' in node):
        return False
    else:
        return True

def valid_UV(node,field_name):
    if is_UV(node):
        return node
    else:
        raise errors.InvalidInputError(field_name, node, 'The provided value should be a dict containing a unit key and a value key')

def normalize_state(steady_state_values: dict):
    state = {'t': 0.,
             'u': steady_state_values['u'],
             'v': steady_state_values['v'],
             'w': steady_state_values['w'],
             'x': 0.,
             'y': 0.,
             'z': steady_state_values['z'],
             'p': 0.,
             'q': 0.,
             'r': 0.,
             'phi': steady_state_values['phi'],
             'theta': steady_state_values['theta'],
             'psi': 0.}
    return state

def get_output_overwrite_mode(config):
    """Parses the output overwrite mode

    Parameters
    ----------
    config: dict
        Configuration parsed as a dict from the 'general' section of the YAML input.

    Returns
    -------
    bool
        True if output file should be overwritten, False otherwise
    """
    if 'overwrite existing files' in config:
        return bool(config['overwrite existing files'])
    else:
        return False

def get_mode(config):
    """Parses the operation mode (VPP or PPP)

    Parameters
    ----------
    config: dict
        Configuration parsed as a dict from the 'general' section of the YAML input.

    Returns
    -------
    Mode
        Operation mode as an instance of enum Mode (Mode.VPP or Mode.PPP)
    """
    if 'mode' in config:
        try:
            return Mode(config['mode'])
        except TypeError as e:
            raise errors.InvalidInputError('config: mode', config['mode'], "Unknown mode. Supported modes are 'VPP' and 'PPP'.") from e
    else:
        return Mode.VPP

def get_engine_command(config):
    """Parses the engine command (if present)

    Parameters
    ----------
    config: dict
        Configuration parsed as a dict from the 'general' section of the YAML input.

    Returns
    -------
    str or None
        Engine command if present, None otherwise
    """
    if 'engine command' in config:
        ret = set()
        if isinstance(config['engine command'], list):
            for command in config['engine command']:
                ret.add(str(command))
        else:
            ret.add(str(config['engine command']))
        return ret
    else:
        return None

def get_steering_command(config):
    """Parses the steering command (if present)

    Parameters
    ----------
    config: dict
        Configuration parsed as a dict from the 'general' section of the YAML input.

    Returns
    -------
    str or None
        Steering command if present, None otherwise
    """
    if 'steering command' in config:
        ret = set()
        if isinstance(config['steering command'], list):
            for command in config['steering command']:
                ret.add(str(command))
        else:
            ret.add(str(config['steering command']))
        return ret
    else:
        return None

def get_ship_speed(config):
    """Parses the ship speed (if present)

    Parameters
    ----------
    config: dict
        Configuration parsed as a dict from the 'general' section of the YAML input.

    Returns
    -------
    float or None
        Ship speed in m/s is present, None otherwise
    """
    if 'ship speed' in config:
        if is_UV(config['ship speed']):
            unit = str(config['ship speed']['unit'])
            value = float(config['ship speed']['value'])
            if unit == 'm/s':
                return value
            elif unit in ['kts', 'kt', 'knot', 'knots']:
                return 0.514444*value
            else:
                raise errors.InvalidInputError('ship speed: unit', unit, "Only 'm/s' and 'kts' (or 'kt', 'knot', 'knots') are currently supported for ship speed.")
        else:
            return float(config['ship speed'])
    else:
        return None

def first(a: set):
    """Returns the 'first' (ambiguous as sets are not ordered) element of a set."""
    return next(iter(a))
