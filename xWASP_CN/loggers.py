import logging
import functools

class StackLogger(logging.Logger):
    """This LoggerAdapter adds information about the computation stage to the logs."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.stack = []

    def push_stack(self, name):
        """Adds a caller name to the stack."""
        self.stack.append(name)

    def pop_stack(self):
        """Removes the latest name in the stack."""
        self.stack.pop()

    def _log(self, level, msg, *args, **kwargs):
        """
        Delegate a log call to the underlying logger, after adding
        contextual information from this adapter instance.
        """
        if self.isEnabledFor(level):
            msg, kwargs = self.process(msg, kwargs)
            super()._log(level, msg, *args, **kwargs)

    def process(self, msg, kwargs):
        if self.stack:
            return '[%s] %s' % ('/'.join(self.stack), msg), kwargs
        else:
            return msg, kwargs

class ParametersLoggerAdapter(logging.LoggerAdapter):
    """This LoggerAdapter adds information about the parameters to the logs.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.stack = []

    def push_stack(self, name):
        """Adds a caller name to the stack."""
        self.stack.append(name)

    def pop_stack(self):
        """Removes the latest name in the stack."""
        self.stack.pop()

    def process(self, msg, kwargs):
        if self.stack:
            return '{TWS:%5.1f, TWA:%5.1f}:[%s] %s' % (self.extra['TWS'], self.extra['TWA'], '/'.join(self.stack), msg), kwargs
        else:
            return '{TWS:%5.1f, TWA:%5.1f} %s' % (self.extra['TWS'], self.extra['TWA'], msg), kwargs

def log_stack(local, name):
    """Decorator that adds a name to local.logger's stack."""
    def decorator_log_stack(func):
        @functools.wraps(func)
        def wrapper_log_stack(*args, **kwargs):
            local.logger.push_stack(name)
            try:
                return func(*args, **kwargs)
            finally:
                local.logger.pop_stack()
        return wrapper_log_stack
    return decorator_log_stack