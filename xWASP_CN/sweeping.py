from threading import Lock
import concurrent.futures
from typing import List, Callable, Any

import numpy as np

from xWASP_CN.data_structures import Parameters, ParametersUnits
from xWASP_CN.errors import InvalidInputError
from xWASP_CN.output import Outputter

class Sweeper:
    def __init__(self, general_input) -> None:
        self.lock = Lock()
        if 'units' in general_input:
            self.units = ParametersUnits.from_dict(general_input['units'])
        else:
            self.units = ParametersUnits()
        self.wind_speeds = np.array(sorted(general_input['wind speeds']), dtype = float)
        self.wind_angles = np.array(sorted(general_input['wind angles']), dtype = float)

        self.max_threads = int(general_input['max number of processes'])

    def run(self, operations: List[Callable[[Parameters], Any]], pre_results=None):
        if self.max_threads>1:
            return self._concurrent_run(operations, pre_results)
        else:
            return self._sequential_run(operations, pre_results)

    def _sequential_run(self, operations: List[Callable[[Parameters], Any]], pre_results=None):
        results = np.ndarray((len(self.wind_speeds),len(self.wind_angles),len(operations)), dtype=object)
        with self.lock:
            for i, wind_speed in enumerate(self.wind_speeds):
                for j, wind_angle in enumerate(self.wind_angles):
                    for k, operation in enumerate(operations):
                        parameters = Parameters(wind_speed, wind_angle, self.units)
                        if pre_results is None:
                            results[i,j,k] = operation(parameters)
                        else:
                            try:
                                pre_res = pre_results[i,j]
                            except IndexError as error:
                                raise InvalidInputError("pre_results", pre_results, "Wrong format, the preliminary results should have as many elements as the computations to be run.") from error
                            results[i,j,k] = operation(parameters, pre_res)
        return results

    def _concurrent_run(self, operations: List[Callable[[Parameters], Any]], pre_results=None):
        results = np.ndarray((len(self.wind_speeds),len(self.wind_angles),len(operations)), dtype=object)
        with self.lock:
            with concurrent.futures.ThreadPoolExecutor(max_workers=self.max_threads) as executor:
                futures = {}
                for i, wind_speed in enumerate(self.wind_speeds):
                    for j, wind_angle in enumerate(self.wind_angles):
                        for k, operation in enumerate(operations):
                            parameters = Parameters(wind_speed, wind_angle, self.units)
                            if pre_results is None:
                                futures[(i,j,k)] = executor.submit(operation, parameters)
                            else:
                                try:
                                    pre_res = pre_results[i,j]
                                except IndexError as error:
                                    raise InvalidInputError("pre_results", pre_results, "Wrong format, the preliminary results should have as many elements as the computations to be run.") from error
                                futures[(i,j,k)] = executor.submit(operation, parameters, pre_res)
                for ijk, future in futures.items():
                    if future.exception():
                        raise future.exception()
                    results[ijk] = future.result()
        return results


    def output_parameters(self, outputter: Outputter):
        outputter.write_parameters(self.wind_speeds, self.wind_angles, self.units)