#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
from threading import local

import yaml
from numpy import vectorize

from xWASP_CN.loggers import  log_stack
from xWASP_CN.statics import Statics
import xWASP_CN.errors as errors
import xWASP_CN.dynamics as dyn
from xWASP_CN.yaml_helpers import get_yaml, get_mode, get_engine_command, get_steering_command, get_ship_speed, get_output_overwrite_mode
from xWASP_CN.sweeping import Sweeper
from xWASP_CN.output import HDF5Outputter

local = local()
local.logger = logging.getLogger('xWASP_CN') # Gets root logger

# To suppress tags when writing a YAML file
def noop(self, *args, **kw):
    pass
yaml.emitter.Emitter.process_tag = noop

@log_stack(local, 'statics')
def run_statics(sweeper,
                xdyn_model_file,
                statics_input,
                mode,
                engine_command,
                steering_command,
                ship_speed,
                wind_model=None,
                outputter=None):
    local.logger.info("Running static computations...")
    statics_computer = Statics(xdyn_model_file, statics_input, mode, engine_command, steering_command, ship_speed, wind_model)
    statics_results = sweeper.run([statics_computer.compute])[:,:,0]
    if outputter is not None:
        statics_computer.output_initial_hydrostatics(outputter)
        outputter.write_statics_results(statics_results)
    local.logger.info('Done computing statics!')
    return statics_results

@log_stack(local, 'dynamics')
def run_dynamics(sweeper,
                 xdyn_model_file,
                 dynamics_input,
                 mode,
                 engine_command,
                 steering_command,
                 statics_results,
                 wind_model=None,
                 outputter=None):
    known_tests = [dyn.ZigZagTest, dyn.TurningCircleTest, dyn.RegularWavesTest, dyn.SpectralWavesTest]
    tests = []
    local.logger.info("Running dynamic computations...")
    dynamics_config = dynamics_input['config']
    for test_input in dynamics_input['computations']:
        for test_class in known_tests:
            match = False
            if test_input['type']==test_class.type():
                tests.append(dyn.instantiate_test(test_class, test_input['name'], xdyn_model_file, dynamics_config, test_input, mode, engine_command, steering_command, wind_model))
                match = True
                break
        if not match:
            raise errors.InvalidInputError('dynamics/computations: type', test_input['type'], "Test type unknown.")

    operations = []
    for test in tests:
        operations.append(test.compute)
    get_static_states = vectorize(lambda x: x.system_states)
    dynamics_results = sweeper.run(operations, get_static_states(statics_results))
    if outputter is not None:
        outputter.write_dynamic_results(dynamics_results)
    local.logger.info('Done computing dynamics!')

def run_tests(wrapper_input, output_file):
    try:
        general_input = wrapper_input['general']
    except KeyError as error:
        raise errors.MissingParameter('general') from error
    try:
        xdyn_model_file = general_input['xdyn model file']
    except KeyError as error:
        raise errors.MissingParameter('xdyn model file', 'general') from error
    mode = get_mode(general_input)
    engine_command = get_engine_command(general_input)
    steering_command = get_steering_command(general_input)
    ship_speed = get_ship_speed(general_input)
    wind_model = None
    if 'wind model' in general_input:
        wind_model = general_input['wind model']
    else:
        local.logger.info("Defaulting wind model to 'uniform wind'")
    parameters_sweeper = Sweeper(general_input)

    overwrite_output_file = get_output_overwrite_mode(general_input)
    if output_file is not None:
        outputter = HDF5Outputter(output_file, overwrite_output_file)
        outputter.write_input(wrapper_input, xdyn_model_file)
        parameters_sweeper.output_parameters(outputter)
    else:
        outputter = None

    statics_results = run_statics(parameters_sweeper, xdyn_model_file, wrapper_input['statics'], mode, engine_command, steering_command, ship_speed, wind_model, outputter)

    if 'dynamics' in wrapper_input and wrapper_input['dynamics'] is not None:
        run_dynamics(parameters_sweeper, xdyn_model_file, wrapper_input['dynamics'], mode, engine_command, steering_command, statics_results, wind_model, outputter)


def run_with_optional_file_logs(wrapper_input, output_file):
    """Wraps run_tests in optional file logging."""
    try:
        general_input = wrapper_input['general']
    except KeyError as error:
        raise errors.MissingParameter('general') from error
    if 'log to file' in general_input:
        numeric_level = getattr(logging, str(general_input['log to file']).upper(), None)
        if not isinstance(numeric_level, int):
            raise errors.InvalidInputError('general: log to file', str(general_input['log to file']), 'Invalid logging level. Supported levels are DEBUG, INFO, WARNING, ERROR and CRITICAL.')
        handler = logging.FileHandler('xWASP_CN.log', mode = 'w', encoding='utf-8')
        handler.setLevel(numeric_level)
        handler.setFormatter(logging.Formatter('[%(levelname)s] %(message)s'))
        local.logger.addHandler(handler)

    try:
        run_tests(wrapper_input, output_file)
    except errors.Error as error:
        local.logger.critical(str(error))
    except KeyboardInterrupt:
        local.logger.critical('The program was manually interrupted.')
    except:
        local.logger.exception('Unkown error occurred. Please create an Issue at gitlab.com/lheea/xwasp_cn/issues with the details of the error (logs and input files). If the logs/input files cannot be disclosed publicly, please contact the maintainers via email.')

def run_wrapper(console_input):
    # For running the wrapper from xdyn_wrapper.py
    try:
        wrapper_input = get_yaml(console_input['input_file'])
    except errors.InvalidInputError:
        raise errors.InvalidInputError('input file', console_input['input_file'], "Input file not found")
    run_with_optional_file_logs(wrapper_input, console_input['output_file'])
