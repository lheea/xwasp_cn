# This image is based on the latest release of xdyn for building xWASP_CN

FROM sirehna/xdyn:v6-1-0

RUN apt-get update -yq && \
    apt-get install \
        --yes \
        --no-install-recommends \
        python3 \
        python3-pip && \
    apt-get autoclean && \
    apt-get autoremove && \
    apt-get clean && \
    rm -rf /tmp/* /var/tmp/* && \
    rm -rf /var/lib/apt/lists

RUN python3 -m pip install --upgrade pip && \
    python3 -m pip install grpcio-tools

ENV GRPC_ENABLE_FORK_SUPPORT=0

RUN cd /usr/proto && \
    python3 -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. cosimulation.proto && \
    python3 -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. model_exchange.proto

ENV PYTHONPATH="${PYTHONPATH}:/usr/proto"

COPY . /opt/xWASP_CN

ARG GIT_HEAD_HASH=missing_hash

RUN sed -i "s/latest_commit_id_placeholder/$GIT_HEAD_HASH/" /opt/xWASP_CN/xWASP_CN/__main__.py

RUN python3 -m pip install /opt/xWASP_CN --compile && \
    rm -rf /opt/xWASP_CN

VOLUME /work

WORKDIR /work

ENTRYPOINT ["xWASP_CN"]

