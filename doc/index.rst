.. xWASP_CN documentation master file, created by
   sphinx-quickstart on Wed Jan 31 15:58:48 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

####################################
Welcome to xWASP_CN's documentation!
####################################

xWASP_CN is a 6-DoFs Dynamic Velocity/Power Prediction Program dedicated to Wind-Assisted Ship Propulsion. It uses the `xdyn ship simulator <https://gitlab.com/sirehna_naval_group/sirehna/xdyn>`__ developed by SIREHNA for forces computation and time integration. xWASP_CN has statics and dynamics capabilities to compute a ship's response to wind and sea conditions.

xWASP_CN was developed at the LHEEA (Laboratoire de recherche en Hydrodynamique, Énergétique et Environnement Atmosphérique, Ecole Centrale de Nantes/CNRS) in Nantes with funding from the French Environment Agency (ADEME) and the Région Pays de la Loire. It is licensed under the `Eclipse Public License v2.0 <https://www.eclipse.org/legal/epl-2.0/>`__.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   quickstart
   tutorial
   input
   statics
   dynamics
   ship-model

