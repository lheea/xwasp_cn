
########
Dynamics
########

Governing equation
------------------

When computing dynamics, the ship's behavior is governed by the Newton-Euler equations in the ship's frame at the center of mass :math:`G`:

.. math::

   \begin{Bmatrix}
       \vec{F} \\
       \vec{M}_G
   \end{Bmatrix}
   =
   \begin{bmatrix}
       m \mathbb{I}_3 & 0 \\
       0 & I_G
   \end{bmatrix}
   \begin{Bmatrix}
       \vec{a}_G \\
       \vec{\alpha}
   \end{Bmatrix}
   +
   \begin{Bmatrix}
       0 \\
       \vec{\omega} \times I_G \vec{\omega}
   \end{Bmatrix}

With:

-  :math:`\vec{F}` the sum of external forces acting on the ship
-  :math:`\vec{M}_G` the sum of external moments acting on the body at :math:`G`
-  :math:`m` the ship's mass
-  :math:`I_G` the ship's inertia matrix at :math:`G`
-  :math:`\vec{a}_G` the acceleration of :math:`G`
-  :math:`\vec{\alpha}` the angular acceleration
-  :math:`\vec{\omega}` the angular velocity

The term :math:`\vec{\omega} \times I_G \vec{\omega}`, known as *fictitious forces*, appears when applying Newton's law of motion into a rotating body frame.

The governing equation is integrated with respect to time by *xdyn*, using classical time integration schemes (see *xdyn*'s documentation for available options).

Dynamics computations
---------------------

xWASP_CN is able to perform a number of dynamics simulation, corresponding to classical ship analysis. Each computation has a set of parameters in addition to the common ``name`` and ``type``. This section describes every type of computation available in xWASP_CN.

All dynamics computations start at the steady state computed beforehand.

.. warning:: For the time being, xWASP_CN only outputs the time series for dynamics computations. The post-processing has to be done manually. Built-in computation of key metrics will be added in the future.

Seakeeping
~~~~~~~~~~~

Common parameters
^^^^^^^^^^^^^^^^^

All seakeeping tests have the following parameters in common:

.. code:: yaml

   computations:
     - name: waves1
       type: regular waves
       T: 10
       H: {unit: m, value: 4}
       waves direction: {unit: deg, value: 30}
       depth: {unit: m, value: 10000}
       stretching: 1.
       fixed propulsion: true
       fixed steering command: true

Parameters details:

-  ``T``: wave period (or peak wave period for spectral waves), in seconds.
-  ``H``: wave height (or significant wave height for spectral waves), as a unit/value pair.
-  ``waves direction``: propagation direction of the waves as described in *xdyn*'s documentation, as a unit/value pair.
-  ``depth``: water depth to use in the wave model, as a unit/value pair.
-  ``stretching``: stretching ``delta`` value to use in the wave model (see *xdyn*'s documentation for details).
-  **[optional]** ``fixed propulsion``: whether to keep the propulsion command at the steady state result (``true``), or to let a controller set this command (the controller needs to be defined in the model). Defaults to ``false``.
-  **[optional]** ``fixed steering``: whether to keep the steering command at the steady state result (``true``), or to let a controller set this command (the controller needs to be defined in the model). Defaults to ``false``.

Regular waves
^^^^^^^^^^^^^

The ``regular waves`` test consists in subjecting the ship to an regular incoming wave until an oscillatory steady state is reached. Currently, only Airy waves are available in *xdyn*, but other models may be implemented in the future.

The steady state is considered to be reached when the average of forces and moments over 10 wave periods is under a set target (this method is allegedly not ideal and may be improved in the future).

.. code:: yaml

   computations:
     - name: waves1
       type: regular waves
       T: 10
       H: {unit: m, value: 4}
       waves direction: {unit: deg, value: 30}
       depth: {unit: m, value: 10000}
       stretching: 1.
       mean value target for the sum of: {forces: 2e3, moments: 3e3}
       fixed propulsion: true
       fixed steering command: true
       max time: 1500

Parameters detail:

-  ``mean value target for the sum of``: target values for the 10-period average of forces and moments, in N and N.m respectively.
-  ``max time``: maximum physical time for the computation. If the target wasn't reached it the allowed time, a warning message is issued and the computation is stopped.

Spectral waves
^^^^^^^^^^^^^^

In the ``spectral waves`` test, the ship is subjected to a wave spectrum for a set duration.

.. code:: yaml

   computations:
     - name: seakeeping1
       type: spectral waves
       T: 10
       H: {unit: m, value: 4}
       waves direction: {unit: deg, value: 30}
       depth: {unit: m, value: 10000}
       stretching: 0.5
       fixed propulsion: true
       fixed steering command: true
       simulation time: 1000
       spectrum: JONSWAP
       gamma: 1.2
       number of periods: 10
       period bounds: {min: 1, max: 20}
       directional spreading: true
       s: 2

Parameters detail:

-  ``simulation time``: total physical time for the computation.
-  ``spectrum``: the type of spectrum to use. Can be one of ``JONSWAP``, ``bretschneider`` and ``pierson-moskowitz``.
-  ``gamma``: value of the ``gamma`` parameters for the JONSWAP spectrum. Not used for other spectra.
-  ``number of periods``: number of periods used for the spectrum discretization\*.
-  ``period bounds``: period boundaries used for the spectrum discretization\*.
-  ``directional spreading``: whether the waves direction is spread across a *cos2s* directional spectrum\*. Defaults to ``false``.
-  ``s``: ``s`` parameter for the directional spreading\*. Not used if ``directional spreading`` is ``false``.

\* See *xdyn*'s documentation for details.

Current limitations
^^^^^^^^^^^^^^^^^^^

The seakeeping tests have a number of limitations for the moment:

-  The course cannot be forcefully maintained because *xdyn* doesn't support blocking the angles directly, so a controller is required for course-keeping. In the absence of a good course-keeping controller, the ship's heading may drift and the wind angle differ from the parameters. Alternatives may be implemented in the future (such as the use a of the ``simple heading controller`` force model, or eventually proper DoF blocking).
-  In PPP mode, a mechanical propulsion model is currently required (either controlled or with a fixed command). A fixed propulsion force will be added as an alternative in the future.

Maneuverability
~~~~~~~~~~~~~~~

Detail about the maneuverability testing procedures can be found `here <https://www.wartsila.com/encyclopedia/term/manoeuvring-tests>`__ (only the zig-zag and turning circle procedures are currently implemented in xWASP_CN). This documentation focuses on the description of input parameters.

Zig-zag test
^^^^^^^^^^^^

.. code:: yaml

   computations:
     - name: man1
       type: zig-zag
       amplitude: {unit: deg, value: 10}
       max time: 1500

Parameters detail:

-  ``amplitude``: rudder angle for the zig-zag procedure, as a unit/value pair. Supported units are ``rad`` and ``deg``.
-  ``max time``: maximum physical time allowed for the ship to reach the target heading after a rudder angle change\*.

\* Mainly here as a safeguard to prevent the computation from going on forever if something wrong occurs.

Turning circle test
^^^^^^^^^^^^^^^^^^^

.. code:: yaml

   computations:
     - name: man2
       type: turning circle
       amplitude: {unit: deg, value: 35}
       max time: 3500

Parameters detail:

-  ``amplitude``: rudder angle for the zig-zag procedure, as a unit/value pair. Supported units are ``rad`` and ``deg``.
-  ``max time``: maximum physical time allowed for the ship to reach each stage of the turning circle\*.

\* Mainly here as a safeguard to prevent the computation from going on forever if something wrong occurs.
