
##########
Quickstart
##########

Docker
------

The fastest way to get xWASP_CN working on your computer is with `Docker <https://www.docker.com/>`__. This is the recommended approach, as all the dependencies are included in the Docker image. Once Docker is installed and running (and set up for Linux containers), you can download the xWASP_CN image:

.. code:: bash

   docker pull registry.gitlab.com/lheea/xwasp_cn

.. note:: By default on Linux systems, the Docker Engine is only accessible to the root user. If Docker is not set up to be used by all users (see `here <https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user>`__), you need to prepend all your commands with ``sudo``.

The xWASP_CN Docker image comes with *xdyn* installed. You can check that the image is listed in your available Docker images with the command ``docker images``.

For easier use, you can tag the xWASP_CN Docker image with a shorter name, e.g. ``xwasp_cn``:

.. code:: bash

   docker tag registry.gitlab.com/lheea/xwasp_cn xwasp_cn

In the following, the xWASP_CN docker image will be referred to as ``xwasp_cn``.

You can then start xWASP_CN from a set of input files:

.. code:: bash

   docker run -it --rm -v $(pwd):/work -w /work xwasp_cn input.yml --output results.h5

For details about the Docker options used in this command, see `the Docker CLI documentation <https://docs.docker.com/engine/reference/commandline/cli/>`__.

.. note:: If you are running Docker on Windows with a PowerShell, replace ``$(pwd)`` with ``${PWD}``.

In this example, the input file ``input.yml`` is located in the current working directory, which is mounted to the Docker container at location ``/work``. All other input files (xdyn YAML input file, mesh file, frequency-domain results file...) must be in this folder or a sub-directory, otherwise Docker will be unable to access them. Additionally, the results will be outputted in the ``results.h5`` file in this example (the ``-o`` option shortcut can replace ``--output``).

Running natively
----------------

xWASP_CN is not currently packaged to be installed with Python on a computer. However it is still possible to run xWASP_CN natively on your computer like any other script. For this, you need to have Python 3 installed, and the xdyn binaries need to be in the PATH (i.e. your OS needs to know where to look for them).

You also need to add protobuf files to communicate with *xdyn*. First, you need to download both files:

* `cosimulation.proto <https://gitlab.com/sirehna_naval_group/sirehna/interfaces/-/raw/master/proto/cosimulation.proto?ref_type=heads&inline=false>`__
* `model_exchange.proto <https://gitlab.com/sirehna_naval_group/sirehna/interfaces/-/raw/master/proto/model_exchange.proto?ref_type=heads&inline=false>`__

Put the two files in a dedicated folder, *e.g.* ``proto/``, and add that folder to the ``PYTHONPATH`` environment variable.

Install the ``grpcio-tools`` package with the following command:

.. code:: bash

   pip install grpcio-tools

Then, generate the Python files from the protobuf files:

.. code:: bash

   cd proto/
   python3 -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. cosimulation.proto
   python3 -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. model_exchange.proto

Download the latest version of xWASP_CN from the `GitLab repository <https://gitlab.com/lheea/xwasp_cn>`__ (e.g. as a `.zip <https://gitlab.com/lheea/xwasp_cn/-/archive/master/xwasp_cn-master.zip>`__ or as a `.tar.gz <https://gitlab.com/lheea/xwasp_cn/-/archive/master/xwasp_cn-master.tar.gz>`__ archive).

You can then install xWASP_CN (from xWASP_CN's directory) with the following command:

.. code:: bash

   pip install .

.. note:: If you plan on editing the code, you can install xWASP_CN in editable mode with the ``-e`` option. This way, you can edit the code and run the updated version without reinstalling it.

.. warning:: xWASP_CN was not tested natively except on a Linux system. A few tweaks in the code might be required to run it natively on Windows and other non-linux systems.

Documentation
-------------

The present documentation can be generated in HTML using `Sphinx <https://www.sphinx-doc.org/en/master/>`__ with the following commands:

.. code:: bash

   cd doc/
   make html

You may also need to install additional Python libraries.
