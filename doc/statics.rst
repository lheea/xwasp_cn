
#######
Statics
#######

Modes
-----

xWASP_CN has two modes: PPP (Power Prediction Program) and VPP (Velocity Prediction Program). These modes correspond to two control strategies of a hybrid ship using both wind and mechanical propulsion systems. These two modes are inherited respectively from naval design and from sailing yacht technology.

Power Prediction Program
~~~~~~~~~~~~~~~~~~~~~~~~

In naval design, power prediction is usually a 1-DoF problem, where the goal is to find the propulsion power required to maintain a ship at a set design speed in order to size the mechanical propulsion system.

This approach can be applied to wind-assisted ship propulsion, but the required mechanical propulsion power will depend on the wind conditions since the amount of wind assistance depends itself on the wind. Moreover, because wind propulsion systems also create side forces, it is useful to extend the problems to other degrees of freedom, most notably leeway.

Velocity Prediction Program
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Velocity Prediction Programs are used in the yacht racing world, either for designing or for rating (handicapping) sailing yachts. They are usually 4-DoFs (excluding heave and trim) but some take into account all 6 degrees of freedom. The main goal is to find the ship's forward speed in different wind conditions.

The VPP approach can also be applied to wind-assisted ship propulsion when the ship speed is unknown rather than fixed. This leads to an entirely different mindset, where the ship's mechanical propulsion is either fixed or nonexistent (pure sailing ship), and the ship's final forward speed depends on the wind conditions.

Statics mathematical problem(s)
-------------------------------

General equation
~~~~~~~~~~~~~~~~

The goal of a statics computation is to find the system states :math:`\eta` that corresponds to a static equilibrium, governed by the following equations (Newton-Euler with zero acceleration):

.. math::

   \sum \vec{F}(\eta) = \vec{0}

.. math::

   \sum \vec{M}_G(\eta) = \vec{0}

At base, we have 6 components (3 forces and 3 moments), so we can solve for a maximum of 6 independent variables in the states vector :math:`\eta`. The state vector :math:`\eta` is a combination of variables to be solved by xWASP_CN. Which components of the equation we solve, as well as the choice of the state vector, depends on the mode (PPP or VPP) and what systems are modeled.

Coupling
~~~~~~~~

The components of the state vector solution are coupled *a priori* by the force models :math:`F`. However the problem is still expected to be diagonally dominant, i.e. each component of the state vector affects one component of the equation substantially more than the others.

This assumption allows us to associate each component of the state vector with the component of the governing equations that it is expected to influence the most. This association is used by the decoupled root-finding procedure to approach the solution before finding the coupled solution.

State vector
~~~~~~~~~~~~

Propulsion problem
^^^^^^^^^^^^^^^^^^

The propulsion problem is related to the forward motion of the ship, associated with the force along the longitudinal axis of the ship (projected into the horizontal plane).

PPP mode
''''''''

In Prediction Power mode, the forward speed of the ship is fixed:

-  If the mechanical propulsion system is modeled, its command (``engine command`` in the input) is added to the state vector and associated with the force along the x-axis in the governing equations.
-  If the mechanical propulsion system is not modeled, the equilibrium can be ignored along the propulsion axis. The propulsive force necessary to maintain the speed is deduced from the residual force along this axis.

VPP mode
''''''''

In Velocity Prediction mode, the forward speed of the ship must be solved. It is associated with the force along the x-axis in the governing equations.

Leeway problem
^^^^^^^^^^^^^^

The leeway problem is related to the side motion of the ship, associated with the force along the horizontal axis normal to the propulsion axis. This side motion is always unknown and must be solved.

Course-keeping problem
^^^^^^^^^^^^^^^^^^^^^^

The course-keeping problem is related to the ship's ability to maintain a steady course. Since wind-induced leeway can heavily influence the course, we actually consider heading here. In xWASP_CN, the heading is always fixed in order to preserve the True Wind Angle:

-  If the steering system is modeled, its command (``steering command`` in the input) is added to the state vector and associated with the moment about the vertical axis in the governing equations.
-  If the steering system is not modeled, the equilibrium can be ignored along the steering axis. The steering moment necessary to maintain the course can be deduced from the residual moment along this axis.

Hydrostatics problem
^^^^^^^^^^^^^^^^^^^^

The hydrostatics problem is related to the heave, heel and trim of the ship. These three variables can be included in the state vector, and associated respectively with the vertical force, heel moment and trim moment components of the governing equations.

Algorithm
---------

Decoupled procedure
~~~~~~~~~~~~~~~~~~~

Classical **vector** root-finding algorithms are very sensitive to initial guesses, and have proved unable to tackle the coupled problem on their own. xWASP_CN features an original root-finding procedure that leverages the diagonal-dominance of the problem. The goal of this procedure is to find an initial guess close enough to the coupled solution so that a coupled algorithm succeeds.

The procedure consists in solving successive decoupled problems using a classical **scalar** root-finding algorithm. For each of the state vector components, the other states are fixed and a root is found with the associated governing equation component as the function.

Coupled solution
~~~~~~~~~~~~~~~~

At the end of each **scalar** root-finding sequence, a **vector** root-finding algorithm is used to solve the coupled problem. The results of the decoupled problems are used as the starting point for the coupled problem. If the **vector** root-finding algorithm does not succeed at finding a satisfactory solution, the decoupled sequence is repeated up to a limited number of times until the **vector** root-finding algorithm succeeds.

.. figure:: figures/flowchart.svg
   :align: center

   Root-finding algorithm
