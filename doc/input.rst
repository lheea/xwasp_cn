
###############
Main input file
###############

General description
-------------------

xWASP_CN is controlled with an input file written in `YAML <https://en.wikipedia.org/wiki/YAML>`__. This file describes the study parameters like the ranges of environmental conditions, as well as numerical parameters like the convergence conditions, time step and time integration scheme.

This file is divided in 3 sections: ``general``, ``statics`` and ``dynamics``. The ``general`` section specifies the general numerical parameters for the computations and the location of the *xdyn* model file. The ``statics`` section defines the parameters for the steady-state simulations. The ``dynamics`` section describes the types of dynamic computation that xWASP_CN should execute.

The ``general`` section
-----------------------

Here is an example of the ``general`` section of an input file:

.. code:: yaml

   general:
       overwrite existing files: false
       log to file: info
       max number of processes: 1
       xdyn model file: input/KVLCC2_sails_controlled.yml
       mode: PPP
       engine command: propeller(rpm)
       steering command: rudder(beta)
       ship speed: {unit: m/s, value: 12}
       units: {wind speed: m/s, wind angle: deg}
       wind model:
        model: uniform wind
       wind speeds: [10,20,30]
       wind angles: [1,30,60,90,120,150,179]

All the parameters are detailed below:

-  ``overwrite existing files``: whether to overwrite the output files if they already exist. If this parameter is set to ``false`` and the output files already exist then a new output file is created with "(1)" at the end of the file name (then "(2)" if "(1)" is already used etc.). Defaults to ``false``.
-  **[optional]** ``log to file``: the logging level of the optional output log file "xWASP_CN.log". Supported levels are ``debug``, ``info``, ``warning``, ``error`` and ``critical`` (see the library `logging <https://docs.python.org/3/library/logging.html>`__). Do not declare it if no output log file is wanted.
-  ``max number of processes``: the maximum number of concurrent instances of *xdyn* that can be used. If xWASP_CN is ran inside a Docker container, the shares of CPUs can also be enforced using the proper argument to the `Docker command <https://docs.docker.com/engine/reference/commandline/run/>`__.
-  ``xdyn model file``: the location of the input YAML file for `xdyn <https://gitlab.com/sirehna_naval_group/sirehna/xdyn>`__. This path can be absolute when running xWASP_CN natively, but should always be relative and in a sub-folder of the ``work`` folder (see :ref:`quickstart:Quickstart`) when running in a Docker container (because the context sent to the Docker daemon only includes sub-directories).
-  ``mode``: the computation mode of xWASP_CN, either ``PPP`` (Power Prediction Program) or ``VPP`` (Velocity Prediction Program).
-  **[optional]** ``engine command``: the command corresponding to mechanical propulsion (e.g. engine or propeller) in the *xdyn* model. If provided, xWASP_CN will solve for this command's value instead of the propulsion power in ``PPP`` mode. Not used in ``VPP`` mode.
-  **[optional]** ``steering command``: the command corresponding the steering device (e.g. rudder) in the *xdyn* model. If provided, xWASP_CN will solve for this command's value instead of the steering moment for course-keeping.
-  ``ship speed``: design ship speed for ``PPP`` mode, as a value/unit pair. Supported units are ``m/s`` and ``knots`` (or ``knot``, ``kt``, ``kts``). Not used in ``VPP`` mode.
-  ``units``: the units used for the wind speed and angle. Supported units are ``m/s`` and ``knots`` (or ``knot``, ``kt``, but **not** ``kts``), and ``rad`` and ``deg`` respectively.
-  **[optional]** ``wind model``: the wind model to be used. See `xdyn's documentation <https://sirehna_naval_group.gitlab.io/sirehna/xdyn/#mod%C3%A8les-de-vent>`__ (in French) for available wind models (``velocity`` and ``direction`` do not need to be set here). If not provided, the default value is a uniform wind.
-  ``wind speeds``: true wind speeds (TWS) for which to execute the steady states and dynamic computations.
-  ``wind angles``: true wind angles (TWA) for which to execute the steady states and dynamic computations.

The ``statics`` section
-----------------------

This section defines the parameters used for the steady state computations.

.. code:: yaml

   statics:
       tolerance: 1e-10
       solve hydrostatics: true
       solve initial hydrostatics: true
       max inner iterations: 100
       max decoupled iterations: 10
       decoupled root-finding algorithm: toms748
       coupled root-finding algorithm: hybr
       sail models: ['sail_1', 'sail_2', 'sail_2']
       propeller models: ['propeller']
       max ship speed: 20.
       max engine command: 300
       max steering command: 1.2
       max trim: {unit: deg, value: 10}
       max heel: {unit: deg, value: 30}
       max sinkage: 5
       always save scalar root-finding debug file: false
       output angle unit: deg

All the parameters are detailed below:

-  **[optional]** ``tolerance``: Tolerance for root-finding algorithms. Defaults to 1e-9.
-  **[optional]** ``solve hydrostatics``: whether to solve or not the hydrostatics for all timesteps. Default is ``true``.
-  **[optional]** ``solve initial hydrostatics``: whether to solve or not the hydrostatics for the first timestep only. Default is ``false``. This command is overrun by ``solve hydrostatics``.
-  **[optional]** ``max inner iterations``: the maximum number of iterations for each root-finding procedure. Defaults to 300.
-  **[optional]** ``max decoupled iterations``: the maximum number of decoupled iterations allowed before a coupled root-finding procedure succeeds. Defaults to 3.
-  **[optional]** ``decoupled root-finding algorithm``: algorithm used for scalar (decoupled) root-finding. Supported algorithms are 'bisect', 'brentq', 'brenth', 'ridder' and 'toms748'. See the `Scipy documentation <https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.root_scalar.html#scipy-optimize-root-scalar>`__ for details. Defaults to 'toms748' (recommended).
-  **[optional]** ``coupled root-finding algorithm``: algorithm used for vector (coupled) root-finding. Supported algorithms are 'hybr', 'lm', 'broyden1', 'broyden2', 'anderson', 'linearmixing', 'diagbroyden', 'excitingmixing', 'krylov' and 'df-sane'. See the `Scipy documentation <https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.root.html#scipy-optimize-root>`__ for details. Defaults to 'hybr' (recommended).
-  **[optional]** ``sail models``: the force models corresponding to wind propulsion systems in the *xdyn* model. If provided, used by xWASP_CN to compute the sail power coverage (part of the propulsion power provided by the sails).
-  **[optional]** ``propeller models``: the force models corresponding to mechanical propulsion systems in the *xdyn* model. Required by xWASP_CN to compute the sail power coverage if mechanical propulsion is modeled (i.e. if ``engine command`` is provided in the ``general`` section).
-  **[optional]** ``max ship speed``: maximum ship speed allowed in ``VPP`` mode. Not used in ``PPP`` mode. Defaults to 100 m/s.
-  ``max engine command``: maximum engine command allowed in ``PPP`` mode if mechanical propulsion is modeled (i.e. if ``engine command`` is provided in the ``general`` section). Not used in ``VPP`` mode.
-  ``max steering command``: maximum steering command allowed if steering device is modeled (i.e. if ``steering command`` is provided in the ``general`` section).
-  **[optional]** ``max trim``: maximum trim allowed in steady state as a value/unit pair. Supported units are ``rad`` and ``deg``. Defaults to 30°.
-  **[optional]** ``max heel``: maximum heel allowed in steady state as a value/unit pair. Supported units are ``rad`` and ``deg``. Defaults to 30°.
-  **[optional]** ``max sinkage``: maximum sinkage allowed in steady state, in meters. Defaults to 10m.
-  **[optional]** ``always save scalar root-finding debug file``: whether to write or not a CSV file for each root-finding computation. Default is ``false``.
-  **[optional]** ``output angle unit``: angle unit in the output file, either ``deg`` or ``rad``. Defaults to ``rad``.


The ``dynamics`` section
------------------------

The ``dynamics`` section is itself divided into two subsections: ``config`` and ``computations``. The ``config`` subsection contains general parameters for time integration, while the ``computations`` subsection is a list of tests to be simulated by xWASP_CN. These currently include seakeeping tests and maneuverability tests. Each test must conform to a format which includes a few common parameters, and also includes its own parameters. Here is an example of a tests section including a zig-zag test and a seakeeping test.

.. code:: yaml

   dynamics:
       config:
           solver: rk4
           dt: 1
           save time series: true
       computations:
         - name: man1
           type: zig-zag
           amplitude: {unit: deg, value: 10}
           max time: 1500

         - name: waves1
           type: regular waves
           T: 10
           H: {unit: m, value: 4}
           waves direction: {unit: deg, value: 30}
           depth: {unit: m, value: 10000}
           stretching: 1.
           mean value target for the sum of: {forces: 2e3, moments: 3e3}
           fixed propulsion: true
           fixed steering command: true
           angle unit: deg
           max time: 1500

``config`` subsection parameters:

-  ``dt``: simulation time step (forwarded to *xdyn*)
-  ``solver``: time integration scheme (forwarded to *xdyn*, refer to its documentation for available options).
-  ``save time series``: whether to save the time history of the states and forces for all dynamic computations.

The following parameters are common to all tests in the ``computations`` section:

-  ``name``: this string is used to refer to the test in the output files. It is especially useful when several tests of the same type are performed.
-  ``type``: this string defines the type of test, and must be one of the supported tests.

The parameters that are specific to each test are detailed in the documentation of this test.
