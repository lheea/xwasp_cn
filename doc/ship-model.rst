
##########
Ship model
##########

The ship model is defined in the ``xdyn model file``, in the YAML format. The complete description of the model file is available in the documentation of *xdyn*. The present page provides an overview of the forces (and other inputs) that need to be modeled for a computation in xWASP_CN.

Statics
-------

The following forces need to be modeled for a statics computation:

+-------------------------+------------------------------------------------------------------+
| Force                   | Example *xdyn* force models                                      |
+=========================+==================================================================+
| Forward resistance      | ``Holtrop & Mennen``, ``resistance curve``                       |
+-------------------------+------------------------------------------------------------------+
| Wind propulsion         | ``aerodynamic polar``                                            |
+-------------------------+------------------------------------------------------------------+
| Mechanical propulsion   | ``wageningen B-series``, ``Kt(J) & Kq(J)``, ``propeller+rudder`` |
+-------------------------+------------------------------------------------------------------+
| Hull side force         | ``MMG maneuvering``, ``maneuvering``, ``hydrodynamic polar``     |
+-------------------------+------------------------------------------------------------------+
| Rudder force            | ``propeller+rudder``                                             |
+-------------------------+------------------------------------------------------------------+
|| Other appendages (e.g. || ``hydrodynamic polar``                                          |
|| centerboard)           ||                                                                 |
+-------------------------+------------------------------------------------------------------+
|| Hydrostatics           || ``hydrostatic``, ``non-linear hydrostatic (exact)``,            |
||                        || ``non-linear hydrostatic (fast)``, ``linear hydrostatics``      |
+-------------------------+------------------------------------------------------------------+
|| Gravity                || ``gravity`` (along with the proper mass and center of           |
||                        || inertia in the ``dynamics`` section)                            |
+-------------------------+------------------------------------------------------------------+

Mechanical propulsion and rudder force may be omitted (see :ref:`statics:Propulsion problem`).

.. note:: For statics computation, a mesh may not be needed. In the mentioned forces, only the non-linear hydrostatics models (``hydrostatic``, ``non-linear hydrostatic (exact)`` and ``non-linear hydrostatic (fast)``) make use of the mesh.

Dynamics
--------

Any dynamics computation needs the ``dynamics`` section (of the *xdyn* model file) to be filled correctly. This includes the **position of the center of inertia**, the **rigid body inertia matrix** and the **added mass matrix** (the later can be retrieved by *xdyn* from the HDB file used for radiation damping and diffraction models).

In addition to all the forces necessary for the statics computations, some force models are also necessary for dynamics computations.

Seakeeping
~~~~~~~~~~~

Necessary forces for seakeeping computations:

================= =========================================
Force             Example *xdyn* force models
================= =========================================
Radiation damping ``radiation damping``
Viscous damping   ``quadratic damping``, ``linear damping``
Diffraction       ``diffraction``
Froude-Krylov     ``non-linear Froude-Krylov``
================= =========================================

Maneuvering
~~~~~~~~~~~

In theory, no other force than in the statics case need to be modeled for maneuvering tests. However in practice, most maneuvering models are only 3-DoFs (surge, sway and yaw), and without damping forces in the other DoFs the simulations would diverge. Until proper DoF-blocking is implemented in *xdyn* for these DoFs, maneuvering simulations in xWASP_CN require a radiation force model or another damping model (e.g. ``quadratic damping`` and/or ``linear damping``). Be careful when using these models as they may change the statics results as well.
