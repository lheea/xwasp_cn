
########
Tutorial
########

This page describes the process to set up a simulation with xWASP_CN and post-process the results, using an example. The main steps are:

-  Building the *xdyn* model
-  Creating the xWASP_CN input file
-  Running the simulation
-  Post-processing

The example files built in this tutorial are available in the ``example`` folder.

Building the *xdyn* model
-------------------------

The xdyn model file (`example <https://gitlab.com/lheea/xwasp_cn/-/blob/master/example/ship_model.yml>`__) describes the ship and the force models acting upon it. A complete description of the model file can be found in *xdyn*'s documentation (including the list of available force models). Detail about what forces to model for each type of simulation can be found in the :ref:`ship-model:Ship model` section.

Rotation convention
~~~~~~~~~~~~~~~~~~~

By itself, *xdyn* allows for all possible combinations of rotations (see *xdyn*'s documentation for a comprehensive list). However, in order to enforce the course, xWASP_CN uses the yaw angle :math:`\psi`. Thus, the convention must always include a single rotation of the angle :math:`\psi` around the fixed (vertical) Z axis (conventions 1-6, 11, **12**, 14, and 18 from the *xdyn* documentation). Regardless of the convention chosen, the results must be interpreted with regards to this convention.

We recommend using the aeronautics convention (id: 12), which is:

1. Rotation of :math:`\psi` around the fixed (vertical) Z axis
2. Rotation of :math:`\theta` around the Y' axis resulting from the first rotation
3. Rotation of :math:`\phi` around the X'' axis resulting from the previous rotations

This convention is defined as follows is the xdyn model file:

.. code:: yaml

   rotations convention: [psi, theta', phi'']

Environmental models
~~~~~~~~~~~~~~~~~~~~

Any environmental model present in the *xdyn* model file will be overwritten by xWASP_CN. This section can be omitted altogether.

Ship parameters
~~~~~~~~~~~~~~~

The ship model itself is written in the ``bodies`` section of the *xdyn* input file. This section is a list because *xdyn* by itself is able to simulate several bodies. However in xWASP_CN, only the ship is of interest, and thus only one body must be described in this section.

.. code:: yaml

   bodies:
     - name: ship
       [...]

The ship can have any name, which may be used in the output to reference the body.

Mesh
^^^^

A mesh may be needed for some force models (including Froude-Krylov and non-linear hydrostatics). If that is the case, the mesh file is specified as well as the position of the body frame relative to the mesh frame:

.. code:: yaml

   bodies:
     - name: ship
       mesh: KVLCC2_mesh.stl
       position of body frame relative to mesh:
           frame: mesh
           x: {value: 0, unit: m}
           y: {value: 0, unit: m}
           z: {value: 0, unit: m}
           phi: {value: 0, unit: rad}
           theta: {value: 0, unit: rad}
           psi: {value: 0, unit: rad}
       [...]

In this example the body frame is located at the origin of the mesh frame.

Initial states
^^^^^^^^^^^^^^

The initial states (initial position and initial velocity) are overwritten by xWASP_CN for dynamics computations, and not used for statics computations. However they must still be present for *xdyn* to initialize the computation.

.. code:: yaml

   bodies:
     - name: ship
       [...]
       initial position of body frame relative to NED:
           frame: NED
           x: {value: 0, unit: m}
           y: {value: 0, unit: m}
           z: {value: 0, unit: m}
           phi: {value: 0, unit: deg}
           theta: {value: 0, unit: deg}
           psi: {value: 0, unit: deg}
       initial velocity of body frame relative to NED:
           frame: ship
           u: {value: 0, unit: m/s}
           v: {value: 0, unit: m/s}
           w: {value: 0, unit: m/s}
           p: {value: 0, unit: rad/s}
           q: {value: 0, unit: rad/s}
           r: {value: 0, unit: rad/s}
       [...]

Inertial properties
^^^^^^^^^^^^^^^^^^^

Inertial properties of the ship are important for hydrostatics and dynamics computations. They are specified in the ``dynamics`` section of the body model.

The ``hydrodynamic forces calculation point`` is used for some forces models (including forward resistance), and thus should be carefully chosen.

If only statics are to be computed, the inertia and added mass matrix have not effect. They must still be specified. However the mass (:math:`I_{1,1} = I_{2,2} = I_{3,3} = m`) must still be provided for a correct hydrostatic equilibrium to be found.

For dynamics computations, the added mass matrix can either be specified directly or retrieved from an HDB file (result of a frequency-domain potential flow seakeeping solver).

.. code:: yaml

   bodies:
     - name: ship
       [...]
       dynamics:
           hydrodynamic forces calculation point in body frame:
               x: {value: 0, unit: m}
               y: {value: 0, unit: m}
               z: {value: 0, unit: m}
           centre of inertia:
               frame: ship
               x: {value: 0, unit: m}
               y: {value: 0, unit: m}
               z: {value: 2.2, unit: m}
           rigid body inertia matrix at the center of gravity and projected in the body frame:
               row 1: [3.101981e+08,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00]
               row 2: [0.000000e+00,3.101981e+08,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00]
               row 3: [0.000000e+00,0.000000e+00,3.101981e+08,0.000000e+00,0.000000e+00,0.000000e+00]
               row 4: [0.000000e+00,0.000000e+00,0.000000e+00,2.087872e+11,1.201552e+09,2.187367e+10]
               row 5: [0.000000e+00,0.000000e+00,0.000000e+00,1.201552e+09,2.421491e+12,5.394442e+08]
               row 6: [0.000000e+00,0.000000e+00,0.000000e+00,2.187367e+10,5.394442e+08,2.464725e+12]
           added mass matrix at the center of gravity and projected in the body frame:
               from hdb: KVLCC2.hdb
       [...]

Forces models
^^^^^^^^^^^^^

The force models are arguably the most important part of the model, and choosing them appropriately will dictate the accuracy of the results. A broad number of models are already included in *xdyn*, but you can implement your own models (either directly in *xdyn*'s code or as remote models using gRPC as an interface).

Each model is related to a specific physical problem, and represents a compromise between accuracy and computation time. Different models for the same force can be useful at different stages of a project. For example, forward resistance can be modeled using increasingly accurate methods:

-  Semi-empirical models such as Holtrop-Mennen
-  Resistance curves

   -  From potential flow computation with ITTC formulae for friction
   -  From CFD computations

-  Fully coupled models (computing the resistance on the fly using a potential flow solver or CFD coupled to *xdyn*)

The latter is more complicated to set up, and requires heavy computational power.

Additionally, some force models may be useful for dynamics computations but useless for statics. This is true regardless of the validity range of the model. For example waves-related models are useless for computing statics. The opposite is rarely true: most forces used for statics need to be computed in dynamics computations as well, but the area of validity must be considered (e.g. a forward resistance designed for statics may not do well in dynamics computations).

General guidelines for force models are given in the :ref:`ship-model:Ship model` section.

Statics
'''''''

Hydrostatics
""""""""""""

Hydrostatics is the sum of buoyancy and gravity (although *xdyn* currently requires the gravity model to be explicitly specified in addition to one of the hydrostatics models).

.. code:: yaml

   bodies:
     - name: ship
       [...]
       external forces:
         - model: gravity
         - model: hydrostatic
       [...]

The hydrostatics model used in this example requires a mesh, but some linear hydrostatics models do not. If sink, heel and trim are expected to be small, a linear model allows for substantial computation time reduction while providing satisfactory results (because integrating over the hull mesh is one of the most time-consuming operation in *xdyn*).

Forward resistance
""""""""""""""""""

Forward resistance, along with propulsion systems, is the main force component that dictate forward motion equilibrium. As mentioned above, there is a variety of models suitable for computing this force. The Holtrop-Mennen model used in this example is a semi-empirical model based on a regression over a few hundred hull shapes, and widely used to get a rough estimation of the forward resistance of classical hull shapes. However it should probably be avoided for innovative hull shapes used in wind-assisted ship propulsion.

.. code:: yaml

   bodies:
     - name: ship
       [...]
       external forces:
         - model: Holtrop & Mennen
           Lwl: {value: 325.5,unit: m}
           Lpp: {value: 320,unit: m}
           B: {value: 58,unit: m}
           Ta: {value: 20.8,unit: m}
           Tf: {value: 20.8,unit: m}
           Vol: {value: 312622,unit: m^3}
           lcb: 3.48
           S: {value: 27194,unit: m^2}
           Abt: {value: 25,unit: m^2}
           hb: {value: 2.5,unit: m}
           Cm: 0.998
           Cwp: 0.83
           At: {value: 0,unit: m^2}
           Sapp: {value: 273.3,unit: m^2}
           Cstern: 0
           1+k2: 2
           apply on ship speed direction: false
       [...]

Mechanical propulsion and steering
""""""""""""""""""""""""""""""""""


Modeling the mechanical propulsion is useful to check that the ship is able to maintain a set speed, and in term compute the engine power and fuel consumption.

Modeling the steering system is useful to check that the ship is able to maintain a set course with a tolerable rudder angle.

These systems are better modeled coupled, as the rudder is often placed in the direct wake of a propeller to benefit from the locally accelerated flow, and thus the flow around the rudder depends on the propeller's rotation speed.

When modeled, these systems need to be controlled. Their commands must be provided to xWASP_CN in the :ref:`input:Main input file`, as the goal of xWASP_CN is to find their static setpoints.

However they need not be modeled for xWASP_CN to work: instead xWASP_CN will find the propulsive force and steering moment that they must provide.

.. code:: yaml

   bodies:
     - name: ship
       [...]
       external forces:
         - name: propellerAndRudder
           model: propeller+rudder
           position of propeller frame:
               frame: mesh(ship)
               x: {value: -163.82, unit: m}
               y: {value: 0, unit: m}
               z: {value: 15, unit: m}
               phi: {value: 0, unit: rad}
               theta: {value: 0, unit: deg}
               psi: {value: 0, unit: deg}
           wake coefficient w: 0.576
           relative rotative efficiency etaR: 1
           thrust deduction factor t: 0.7
           rotation: clockwise
           number of blades: 5
           blade area ratio AE/A0: 0.431
           diameter: {value: 9.326, unit: m}
           rudder area: {value: 136.7, unit: m^2} #{value: 273.3, unit: m^2}
           rudder height: {value: 15.8, unit: m^2}
           effective aspect ratio factor: 1.7
           lift tuning coefficient: 2.1
           drag tuning coefficient: 1
           position of rudder in body frame:
               x: {value: -170, unit: m}
               y: {value: 0, unit: m}
               z: {value: 10, unit: m}
       [...]

   commands:
     - name: propellerAndRudder
       t: [0]
       P/D: {unit: none, values: [0.721]}

.. note:: The propeller model used in this example has a variable blade pitch. This parameter cannot be passed to xWASP_CN, because there can be only one mechanical propulsion command (otherwise there would be more unknowns than equations). Thus, the blade pitch ``P/D`` needs to be provided as a command in the *xdyn* model file. Here it is set at ``0.721`` for all times.

Wind propulsion
"""""""""""""""

Currently, the only built-in model for wind propulsion systems in *xdyn* is the ``aerodynamic polar``. It uses lift and drag coefficients, which depend on the angle of attack. This model can be used to represent any kind of wind propulsion system, and even the drag caused by superstructures, as long as is is proportional to the square of the relative wind velocity.

The lift and drag coefficients values used in this example are for a soft mainsail, from `ORC-VPP's documentation <https://orc.org/uploads/files/ORC-VPP-Documentation-2023.pdf>`__.

.. code:: yaml

   bodies:
     - name: ship
       [...]
       external forces:
         - name : sail_1
           model : aerodynamic polar
           calculation point in body frame:
               x: {value: 0, unit: m}
               y: {value: 0, unit: m}
               z: {value: -21, unit: m}
               phi: {value: 0, unit: rad}
               theta: {value: 0, unit: deg}
               psi: {value: 0, unit: deg}
           reference area : {value: 1000, unit: m^2}
           AWA : {unit: deg, values: [0,7,9,12,28,60,90,120,150,180]}
           lift coefficient : [0.00000,0.94828,1.13793,1.25000,1.42681,1.38319,1.26724,0.93103,0.38793,-0.11207]
           drag coefficient : [0.03448,0.01724,0.01466,0.01466,0.02586,0.11302,0.38250,0.96888,1.31578,1.34483]
       [...]

.. note:: The example file actually includes 3 sails located along the ship. For brevity, only one sail was presented here.

Side forces
"""""""""""

In addition to the propulsive force, wind propulsion systems usually produce a side force that results in leeway. The find the amount of leeway, there must be a force model to represent the hull's resistance to side motion. Side forces are not traditionally taken into account for propulsion power assessment, so finding suitable models is more difficult.

One solution is to use maneuverability models when the data is available. Maneuverability coefficients may also be obtained through CFD beforehand, or derived from semi-empirical models (not currently included in *xdyn*, and not always suitable for innovative hull shapes). These types of models need to be used with care, as they are usually only derived for the horizontal plane, and thus will not take into account heel, trim and heave.

.. code:: yaml

   bodies:
     - name: ship
       [...]
       external forces:
         - model: MMG maneuvering
           calculation point in body frame:
               x: {value: -11.1, unit: m}
               y: {value: 0, unit: m}
               z: {value: 0, unit: m}
           Lpp: {value: 320, unit: m}
           T: {value: 20.8, unit: m}
           Xvv: -0.04
           Xrr: 0.011
           Xvr: 0.002
           Xvvvv: 0.771
           Yv: -0.315
           Yr: 0.083
           Yvvv: -1.607
           Yrvv: 0.379
           Yvrr: -0.391
           Yrrr: 0.008
           Nv: -0.137
           Nr: -0.049
           Nvvv: -0.03
           Nrvv: -0.294
           Nvrr: 0.055
           Nrrr: -0.013
       [...]

.. note:: Not all coefficients are necessary for statics computations. All coefficients related to the yaw rotation speed :math:`r` can be set to zero, because :math:`r = 0` in all statics computations.

.. warning:: Maneuverability models also take into account the increase in forward resistance induced by leeway. If you use a forward resistance model that already take into account this effect, the coefficients ``Xvv`` and ``Xvvvv`` should be set to 0.

Dynamics
''''''''

Maneuverability
"""""""""""""""

Maneuverability computations require specific models such as the one described in the :ref:`tutorial:Side forces` section.

Since these models are usually restricted to the horizontal plane (surge, sway and yaw), the other degrees of freedom must either be blocked or proper damping be added, or these DoFs (roll, pitch and heave) might oscillate and/or diverge.

Seakeeping
"""""""""""

Seakeeping computations usually require preliminary data. The Froude-Krylov force, resulting from the direct integration of the incident wave pressure field over the ship hull (which requires a mesh), can be computed by *xdyn*. However diffraction and radiation forces require to solve the flow of the body subjected to waves. Those can be obtained using the open-source software `Nemoh <https://lheea.ec-nantes.fr/valorisation/logiciels-et-brevets/nemoh-presentation>`__ or any other commercial option (be careful of the system of axes, as they can vary depending on the software and need to be converted to *xdyn*'s system of axes).

.. code:: yaml

   bodies:
     - name: ship
       [...]
       external forces:
         - model: non-linear Froude-Krylov
         - model: diffraction
           hdb: KVLCC2.hdb
           calculation point in body frame:
               x: {value: 0, unit: m}
               y: {value: 0, unit: m}
               z: {value: 0, unit: m}
           mirror for 180 to 360: true
         - model: radiation damping
           hdb: KVLCC2.hdb
           type of quadrature for cos transform: simpson
           type of quadrature for convolution: simpson
           nb of points for retardation function discretization: 100
           omega min: {value: 0.01, unit: rad/s}
           omega max: {value: 8, unit: rad/s}
           tau min: {value: 0, unit: s}
           tau max: {value: 80, unit: s}
           forward speed correction: true
           suppress constant part: true
           output Br and K: false
           calculation point in body frame:
               x: {value: 0, unit: m}
               y: {value: 0, unit: m}
               z: {value: 0, unit: m}
       [...]

Creating the xWASP_CN input file
--------------------------------

The xWASP_CN input file (`example <https://gitlab.com/lheea/xwasp_cn/-/blob/master/example/wrapper_input.yaml>`__) specifies the type of computations to be perform, the associated numerical parameters, as well as the environmental conditions the ship is to be subjected. The complete description of the xWASP_CN input file can be found in the :ref:`input:Main input file` section.

Running the simulation
----------------------

Please refer to the :ref:`quickstart:Quickstart` page for information about how to launch a computation.

Post-processing
---------------

The results are currently stored in a `HDF5 file <https://www.hdfgroup.org/solutions/hdf5/>`__. This file can be opened and explored using a HDF file explorer such as `HDFView <https://www.hdfgroup.org/downloads/hdfview/>`__, which also provides plotting capabilities. The results can then easily be copied from the HDF5 tables to a spreadsheet, or accessed using the HDF5 library (available in many programming languages).

Future developments of xWASP_CN will include CSV outputting (and other useful formats if requested).
