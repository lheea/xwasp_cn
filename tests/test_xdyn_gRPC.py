import pytest

from xWASP_CN import xdyn_gRPC
from xWASP_CN.data_structures import ForceLabel

class TestXdynServerSteady:
    """This is a test class for xWASP_CN.xdyn_gRPC.Xdyn_server_steady"""

    @pytest.fixture(scope='class')
    def xdyn(self, datadir):
        with xdyn_gRPC.Xdyn_server_steady(datadir / 'floating_cube.yml', {}, datadir) as model:
            yield model

    def test_sum_of_forces(self, xdyn):
        """Checks the sum of forces for an arbitrary state."""
        states = {'u': 0.,
                    'v': 0.,
                    'w': 0.,
                    'x': 0.,
                    'y': 0.,
                    'z': 0.,
                    'phi': 0.,
                    'theta': 0.,
                    'psi': 0.,
                    'p': 0.,
                    'q': 0.,
                    'r': 0.}
        axes = ['Fx', 'Fy', 'Fz', 'Mx', 'My', 'Mz']
        eps = 1e-7 # Relative error
        sum_of_forces = dict(zip(axes, xdyn.sum_of_forces(states, axes)))
        assert sum_of_forces['Fx'] == pytest.approx(0., eps)
        assert sum_of_forces['Fy'] == pytest.approx(0., eps)
        assert sum_of_forces['Fz'] == pytest.approx(0., eps)
        assert sum_of_forces['Mx'] == pytest.approx(0., eps)
        assert sum_of_forces['My'] == pytest.approx(0., eps)
        assert sum_of_forces['Mz'] == pytest.approx(0., eps)
        states['z'] = 1.5 # Immersed cube
        sum_of_forces = dict(zip(axes, xdyn.sum_of_forces(states, axes)))
        assert sum_of_forces['Fx'] == pytest.approx(0., eps)
        assert sum_of_forces['Fy'] == pytest.approx(0., eps)
        assert sum_of_forces['Fz'] == pytest.approx(-500.*9.8067, eps)
        assert sum_of_forces['Mx'] == pytest.approx(0., eps)
        assert sum_of_forces['My'] == pytest.approx(0., eps)
        assert sum_of_forces['Mz'] == pytest.approx(0., eps)
        states['z'] = -1.5 # Cube above water
        sum_of_forces = dict(zip(axes, xdyn.sum_of_forces(states, axes)))
        assert sum_of_forces['Fx'] == pytest.approx(0., eps)
        assert sum_of_forces['Fy'] == pytest.approx(0., eps)
        assert sum_of_forces['Fz'] == pytest.approx(500.*9.8067, eps)
        assert sum_of_forces['Mx'] == pytest.approx(0., eps)
        assert sum_of_forces['My'] == pytest.approx(0., eps)
        assert sum_of_forces['Mz'] == pytest.approx(0., eps)

    def test_individual_forces(self, xdyn):
        """Checks the gravity and buoyancy forces for an arbitrary state."""
        states = {'u': 0.,
                    'v': 0.,
                    'w': 0.,
                    'x': 0.,
                    'y': 0.,
                    'z': 0.,
                    'phi': 0.,
                    'theta': 0.,
                    'psi': 0.,
                    'p': 0.,
                    'q': 0.,
                    'r': 0.}
        eps = 1e-7 # Relative error
        full_states = xdyn.get_system_states(states)
        assert full_states.forces[ForceLabel('Fx', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fy', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fz', 'gravity', 'cube', 'NED')] == pytest.approx(500*9.8067, eps)
        assert full_states.forces[ForceLabel('Mx', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('My', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Mz', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fx', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fy', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fz', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(-500*9.8067, eps)
        assert full_states.forces[ForceLabel('Mx', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('My', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Mz', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        states['z'] = 1.5 # Immersed cube
        full_states = xdyn.get_system_states(states)
        assert full_states.forces[ForceLabel('Fx', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fy', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fz', 'gravity', 'cube', 'NED')] == pytest.approx(500*9.8067, eps)
        assert full_states.forces[ForceLabel('Mx', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('My', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Mz', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fx', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fy', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fz', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(-1000*9.8067, eps)
        assert full_states.forces[ForceLabel('Mx', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('My', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Mz', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        states['z'] = -1.5 # Cube above water
        full_states = xdyn.get_system_states(states)
        assert full_states.forces[ForceLabel('Fx', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fy', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fz', 'gravity', 'cube', 'NED')] == pytest.approx(500*9.8067, eps)
        assert full_states.forces[ForceLabel('Mx', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('My', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Mz', 'gravity', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fx', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fy', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Fz', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Mx', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('My', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)
        assert full_states.forces[ForceLabel('Mz', 'non-linear hydrostatic (fast)', 'cube', 'NED')] == pytest.approx(0., eps)

class TestXdynServerStateful:
    """This is a test class for xWASP_CN.xdyn_gRPC.Xdyn_server_stateful"""

    @pytest.fixture(scope='class')
    def xdyn(self, datadir):
        with xdyn_gRPC.Xdyn_server_stateful(datadir / 'free_fall.yml', {'solver': 'rk4', 'dt': 0.1}, datadir, output_all_forces=True) as model:
            yield model

    def test_simulate(self, xdyn):
        """Checks the time integration for a simple free falling object."""
        xdyn.simulate(10.)
        eps = 1e-12 # Relative error, required to account for integration error
        assert xdyn.get_time() == 10.
        history = xdyn.get_all()
        for i in range(len(history.time)):
            t = i*0.1
            assert history.time[i] == pytest.approx(t, eps)
            assert history.states['x'][i] == 0.
            assert history.states['y'][i] == 0.
            assert history.states['z'][i] == pytest.approx(0.5*9.8067*t**2, eps)
            assert history.states['u'][i] == 0.
            assert history.states['v'][i] == 0.
            assert history.states['w'][i] == pytest.approx(9.8067*t, eps)
            assert history.states['phi'][i] == 0.
            assert history.states['theta'][i] == 0.
            assert history.states['psi'][i] == 0.
            assert history.states['p'][i] == 0.
            assert history.states['q'][i] == 0.
            assert history.states['r'][i] == 0.
        xdyn.reset()
