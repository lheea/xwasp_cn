import pytest
from unittest.mock import Mock

from numpy.random import rand
from numpy import array
from numpy.testing import assert_array_equal

from xWASP_CN.data_structures import Parameters, ParametersUnits
from xWASP_CN import sweeping

class TestSweeper:
    @pytest.fixture
    def general_input(self):
        return {'units': {'wind speed': 'm/s', 'wind angle': 'deg'},
                'wind speeds': [10, 15, 20, 25],
                'wind angles': [0,30,60,90,120,150,180],
                'max number of processes': 3}

    def test_sequential_run(self, general_input):
        sweeper = sweeping.Sweeper(general_input)
        sweeper.max_threads = 1
        op1 = Mock()
        op2 = Mock()
        op3 = Mock()

        res = sweeper.run([op1, op2, op3])

        assert res.shape == (4, 7, 3)
        assert op1.call_count == 4*7
        assert op2.call_count == 4*7
        assert op3.call_count == 4*7

        units = ParametersUnits.from_dict(general_input['units'])
        for ws in general_input['wind speeds']:
            for wd in general_input['wind angles']:
                params = Parameters(ws, wd, units)
                op1.assert_any_call(params)
                op2.assert_any_call(params)
                op3.assert_any_call(params)

    def test_sequential_run_with_pre_results(self, general_input):
        sweeper = sweeping.Sweeper(general_input)
        sweeper.max_threads = 1
        op1 = Mock()
        op2 = Mock()
        op3 = Mock()
        pre_results = rand(4,7)

        res = sweeper.run([op1, op2, op3], pre_results)

        assert res.shape == (4, 7, 3)
        assert op1.call_count == 4*7
        assert op2.call_count == 4*7
        assert op3.call_count == 4*7

        units = ParametersUnits.from_dict(general_input['units'])
        for i, ws in enumerate(general_input['wind speeds']):
            for j, wd in enumerate(general_input['wind angles']):
                params = Parameters(ws, wd, units)
                op1.assert_any_call(params, pre_results[i,j])
                op2.assert_any_call(params, pre_results[i,j])
                op3.assert_any_call(params, pre_results[i,j])

    def test_concurrent_run(self, general_input):
        sweeper = sweeping.Sweeper(general_input)
        sweeper.max_threads = 3
        op1 = Mock()
        op2 = Mock()
        op3 = Mock()

        res = sweeper.run([op1, op2, op3])

        assert res.shape == (4, 7, 3)
        assert op1.call_count == 4*7
        assert op2.call_count == 4*7
        assert op3.call_count == 4*7

        units = ParametersUnits.from_dict(general_input['units'])
        for ws in general_input['wind speeds']:
            for wd in general_input['wind angles']:
                params = Parameters(ws, wd, units)
                op1.assert_any_call(params)
                op2.assert_any_call(params)
                op3.assert_any_call(params)

    def test_concurrent_run_with_pre_results(self, general_input):
        sweeper = sweeping.Sweeper(general_input)
        sweeper.max_threads = 3
        op1 = Mock()
        op2 = Mock()
        op3 = Mock()
        pre_results = rand(4,7)

        res = sweeper.run([op1, op2, op3], pre_results)

        assert res.shape == (4, 7, 3)
        assert op1.call_count == 4*7
        assert op2.call_count == 4*7
        assert op3.call_count == 4*7

        units = ParametersUnits.from_dict(general_input['units'])
        for i, ws in enumerate(general_input['wind speeds']):
            for j, wd in enumerate(general_input['wind angles']):
                params = Parameters(ws, wd, units)
                op1.assert_any_call(params, pre_results[i,j])
                op2.assert_any_call(params, pre_results[i,j])
                op3.assert_any_call(params, pre_results[i,j])

    def test_output_parameters(self, general_input):
        sweeper = sweeping.Sweeper(general_input)
        outputter = Mock()
        sweeper.output_parameters(outputter)
        units = ParametersUnits.from_dict(general_input['units'])
        outputter.write_parameters.assert_called_once()
        assert_array_equal(outputter.write_parameters.call_args.args[0], array(general_input['wind speeds']))
        assert_array_equal(outputter.write_parameters.call_args.args[1], array(general_input['wind angles']))
        assert outputter.write_parameters.call_args.args[2] == units