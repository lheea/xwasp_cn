from math import pi
import pytest
from pathlib import Path

from yaml import safe_load
from deepdiff import DeepDiff

from xWASP_CN import yaml_helpers
from xWASP_CN import errors

def test_convert_wind_direction():
    """Tests the conversion from TWA to wind direction in xdyn (for a ship heading due North)."""
    assert yaml_helpers.convert_wind_direction(0.) == 180.
    assert yaml_helpers.convert_wind_direction(30.) == 210.
    assert yaml_helpers.convert_wind_direction(60.) == 240.
    assert yaml_helpers.convert_wind_direction(90.) == 270.
    assert yaml_helpers.convert_wind_direction(120.) == 300.
    assert yaml_helpers.convert_wind_direction(150.) == 330.
    assert yaml_helpers.convert_wind_direction(180.) == 0.
    assert yaml_helpers.convert_wind_direction(-30.) == 150.
    assert yaml_helpers.convert_wind_direction(-60.) == 120.
    assert yaml_helpers.convert_wind_direction(-90.) == 90.
    assert yaml_helpers.convert_wind_direction(-120.) == 60.
    assert yaml_helpers.convert_wind_direction(-150.) == 30.
    assert yaml_helpers.convert_wind_direction(-180.) == 0.

def test_parse_angle_unit():
    """Tests the parsing of an angle unit."""
    assert yaml_helpers.parse_angle_unit('deg') == 'deg'
    assert yaml_helpers.parse_angle_unit('rad') == 'rad'
    with pytest.raises(errors.InvalidInputError):
        yaml_helpers.parse_angle_unit('this_is_an_unsupported_unit')

def test_get_as_radians():
    """Tests the conversion from any angle unit to radians (only radians and degrees are supported)."""
    assert yaml_helpers.get_as_radians({'value': 0.5, 'unit': 'rad'}) == 0.5
    assert yaml_helpers.get_as_radians({'value': 90., 'unit': 'deg'}) == pi/2.
    with pytest.raises(errors.InvalidInputError):
        yaml_helpers.get_as_radians({'value': 10, 'unit': 'this_is_an_unsupported_unit'})

def test_valid_UV():
    """Tests the check for valid unit/value dict."""
    assert yaml_helpers.valid_UV({'value': 0.5, 'unit': 'rad'}, 'placeholder') == {'value': 0.5, 'unit': 'rad'}
    with pytest.raises(errors.InvalidInputError):
        yaml_helpers.valid_UV({'vlue': 10, 'unt': 'deg'}, 'placeholder')

def test_get_yaml(datadir):
    """Tests the function used to read YAML files from a file name, a file identifier, etc..."""
    test_file_str = str(datadir / 'input.yml')
    reference = safe_load(test_file_str)
    assert DeepDiff(yaml_helpers.get_yaml(reference), reference)
    assert DeepDiff(yaml_helpers.get_yaml(test_file_str), reference)
    assert DeepDiff(yaml_helpers.get_yaml(Path(test_file_str)), reference)
    with pytest.raises(errors.InvalidInputError):
        yaml_helpers.get_yaml(12.5)
