import pathlib
import pytest

@pytest.fixture(scope='module')
def datadir(request):
    """Provides the path to the folder containing test data."""
    return pathlib.Path(request.fspath.dirname) / 'data'