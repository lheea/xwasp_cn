#!/bin/bash

# Usage from repository root: ./.ci-cd/update-registry-images.sh <project-registry-url>
# Note: requires being logged in to the repository with Docker

set -e

PROJECT_REGISTRY="${1:-registry.gitlab.com/lheea/xwasp_cn}"

for TARGET in sphinx; do
    docker build -f .ci-cd/Dockerfile -t $PROJECT_REGISTRY/$TARGET --target $TARGET --ssh default .
    docker push $PROJECT_REGISTRY/$TARGET
done
