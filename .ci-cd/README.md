## CI/CD Docker images

Available images:
- `sphinx`: image containing Python, Sphinx and all documentation dependencies

## Build the Docker images

From **the root of the repository**:

```bash
docker build -f .ci-cd/Dockerfile -t <image> --target <image> .
```

Where `<image>` is one of the available images listed above.

## Update the images on GitLab

### Log in to the registry

```bash
docker login <registry>
```

Where `<registry>` is the base URL of the registry (e.g. `registry.gitlab.com`)

### Tag the images

Assuming the images have been built according to the first sections of the document:

```bash
docker <image> <project-registry>/<image>
```

Where `<project-registry>` is the full URL of the project's registry (e.g. `registry.gitlab.com/lheea/xwasp_cn`)

### Push the images

```bash
docker push <project-registry>/<image>
```

### Quick update

Assuming the Docker client is already logged in to the registry, run the following command from the repository root to update all the images:

```bash
.ci-cd/update-registry-images.sh
```
